import Meteor from 'react-native-meteor';

export default function () {
    Meteor.connect('wss://www.gabmessenger.chat/websocket');
    // Meteor.connect('ws://localhost:3000/websocket');
    // Meteor.connect('ws://10.0.1.3:3000/websocket');
}
