import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import moment from 'moment';

import Avatar from './Avatar';
import Bubble from './Bubble';
import Day from './Day';

export default class Message extends React.Component {

  isSameDay(currentMessage = {}, diffMessage = {}) {
    let diff = 0;
    if (diffMessage.a && currentMessage.a) {
      diff = Math.abs(moment(diffMessage.a).startOf('day').diff(moment(currentMessage.a).startOf('day'), 'days'));
    } else {
      diff = 1;
    }
    if (diff === 0) {
      return true;
    }
    return false;
  }

  isSameUser(currentMessage = {}, diffMessage = {}) {
    if (diffMessage._F && currentMessage._F) {
      if (diffMessage._F === currentMessage._F) {
        return true;
      }
    }
    return false;
  }

  renderDay() {
    if (this.props.currentMessage.a) {
      const {containerStyle, ...other} = this.props;
      const dayProps = {
        ...other,
        isSameUser: this.isSameUser,
          isSameDay: this.isSameDay,
      };
    if (this.props.renderDay) {
      return this.props.renderDay(dayProps);
    }
    return <Day {...dayProps} />;
  }
    return null;
  }

renderBubble() {
  const {containerStyle, ...other} = this.props;
  const bubbleProps = {
      ...other,
    isSameUser: this.isSameUser,
      isSameDay: this.isSameDay,
    };
if (this.props.renderBubble) {
  return this.props.renderBubble(bubbleProps);
}
return <Bubble {...bubbleProps} />;
  }

renderAvatar() {
  // Surya
  // if (this.props.user._F !== this.props.currentMessage._F) {
  //   const {containerStyle, ...other} = this.props;
  //   const avatarProps = {
  //     ...other,
  //     isSameUser: this.isSameUser,
  //     isSameDay: this.isSameDay,
  //   };

  //   return <Avatar {...avatarProps}/>;
  // }
  return null;
}

render() {
  return (
    <View>
      {this.renderDay()}
      <View style={[styles[this.props.position].container, {
        marginBottom: this.isSameUser(this.props.currentMessage, this.props.nextMessage) ? 2 : 10,
      }, this.props.containerStyle[this.props.position]]}>
        {this.props.position === 'left' ? this.renderAvatar() : null}
        {this.renderBubble()}
        {this.props.position === 'right' ? this.renderAvatar() : null}
      </View>
    </View>
  );
}
}

const styles = {
  left: StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'flex-end',
      justifyContent: 'flex-start',
      marginLeft: 8,
      marginRight: 0,
    },
  }),
  right: StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'flex-end',
      justifyContent: 'flex-end',
      marginLeft: 0,
      marginRight: 8,
    },
  }),
};

Message.defaultProps = {
  renderAvatar: null,
  renderBubble: null,
  renderDay: null,
  position: 'left',
  currentMessage: {},
  nextMessage: {},
  previousMessage: {},
  user: {},
  containerStyle: {},
};

Message.propTypes = {
  renderAvatar: React.PropTypes.func,
  renderBubble: React.PropTypes.func,
  renderDay: React.PropTypes.func,
  position: React.PropTypes.oneOf(['left', 'right']),
  currentMessage: React.PropTypes.object,
  nextMessage: React.PropTypes.object,
  previousMessage: React.PropTypes.object,
  user: React.PropTypes.object,
  containerStyle: React.PropTypes.shape({
    left: View.propTypes.style,
    right: View.propTypes.style,
  }),
};
