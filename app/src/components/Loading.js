import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform
} from 'react-native';

import { Actions } from "react-native-router-flux";
import ImagePicker from 'react-native-image-picker';
import { RNS3 } from 'react-native-aws3';

import Meteor, { createContainer } from 'react-native-meteor';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../common/styles';
import Values from '../common/values';
import Texts from '../common/texts';

export default class Loading extends Component {
    constructor(props) {
        super(props);

    }


    render() {

        return (
            <View style={[styles.container]}>
                <Image source={require('../images/logo.png')} style={styles.logo} />
                <Image source={require('../images/loading-dots.gif')} style={styles.loading} />
            </View>
        );
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#a8ffa8',
        height: Values.height
    },
    logo: {
        width: Values.width / 2,
        height: Values.width / 2
    },
    loading: {
        // marginTop: -Values.width / 2,
        width: Values.width / 4,
        height: Values.width / 4,
    }
});



