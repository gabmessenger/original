import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableHighlight
} from 'react-native';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';

export default class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value,
            highlight: {}
        }

        this.onBlur = this.onBlur.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            value: nextProps.value
        });
    }
    
    // <Text style={[Styles.label, this.props.labelStyle]}>{ this.props.label }</Text>
    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.inputContainer, this.state.highlight, this.props.multiline ? styles.multiline : styles.oneline, this.props.inputContainerStyle]}>
                    <TextInput
                        style={[styles.input, this.props.inputStyle ]}
                        value={this.state.value}
                        placeholder={ this.props.placeholder }
                        placeholderTextColor={Values.input.placeholderTextColor}
                        onChangeText={(text) => this.updateValue(text) }
                        onBlur={ this.onBlur }
                        onFocus={() => this.onFocus()}
                        multiline={this.props.multiline ? this.props.multiline : false}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        keyboardType={this.props.keyboardType ? this.props.keyboardType : 'default'}
                    />
                </View>
            </View>
        );
    }

    updateValue(text) {
        if (this.props.onChange) {
            this.setState({ value: text })
            this.props.onChange(text);
        }
    }


    onFocus() {
        this.setState({
            highlight: {
                borderColor: Values.colors.blue,
                // backgroundColor: Values.colors.white,
                borderWidth: this.props.multiline ? 1 : 0,
                borderBottomWidth: this.props.multiline ? 0 : 1,
            }
        });
    }

    onBlur() {
        this.setState({
            highlight: {}
        });
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 0,
        // backgroundColor: Values.colors.background
    },
    label: {
        flex: 1
    },
    inputContainer: {
        // backgroundColor: '#00f',
        marginTop: 10,
        marginBottom: 10,
        height: 31,
        borderWidth: 0,
    },
    oneline: {
        borderColor: Values.input.borderColor,
        borderWidth: 0,
        borderBottomWidth: 1
    },
    multiline: {
        // borderColor: Values.input.borderColor,
        // borderWidth: 1
    },
    input: {
        // backgroundColor: '#f00', //Values.colors.background,
        color: Values.input.color,
        padding: 5,
        alignSelf: 'center',
        height: 30,
        fontSize: Values.input.fontSize,
        width: Values.input.fullWidth,
        borderWidth: 0,
    },
})
