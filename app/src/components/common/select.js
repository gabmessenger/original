// https://js.coach/react-native/react-native-modal-picker?sort=popular&filters=android&category=contacts

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableHighlight,
    TouchableWithoutFeedback,
    ListView,
    Modal
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';

const dummy = [{ "name": "Select" }];

export default class Select extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value,
            text: this.props.text,
            label: this.props.label,
        }

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            visible: nextProps.visible,
            text: nextProps.text,
            value: nextProps.value
        })
    }

    //<Text style={[Styles.label, styles.selectLabel]}>{ this.state.label }</Text>
    render() {
        return (
            <View style={styles.container}>

                <TouchableWithoutFeedback onPress={this.props.selectList}>
                    <View style={styles.selectContainer}>
                        <View style={styles.selectBox}>
                            <Text style={[styles.selectBoxLabel, this.state.text ? { } : { color: Values.input.placeholderTextColor }]}>{this.state.text ? this.state.text : this.state.label}</Text>
                            <MaterialIcons name='chevron-right' size={26} color='#ccc' style={styles.selectBoxIcon} />
                        </View>
                    </View>
                </TouchableWithoutFeedback>

            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 0
    },
    selectContainer: {
    },
    selectBox: {
        flexDirection: 'row',
        backgroundColor: Values.colors.background, //Values.input.backgroundColor,        
        borderColor: Values.input.borderColor,
        borderWidth: 0,
        borderBottomWidth: 1,
        // borderRadius: 3,
        marginTop: 10,
        marginBottom: 10,
        height: 31,
        flex: 1
    },
    selectLabel: {
        alignSelf: 'flex-start'
    },
    selectBoxLabel: {
        padding: 5,
        alignSelf: 'center',
        height: 30,
        flex: 1,
        marginRight: -26,
        fontSize: 16,
        color: Values.input.color,
    },
    selectBoxIcon: {
        height: 25
    },
})


const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });