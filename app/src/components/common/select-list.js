// https://js.coach/react-native/react-native-modal-picker?sort=popular&filters=android&category=contacts

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableHighlight,
    TouchableWithoutFeedback,
    ListView,
    Modal
} from 'react-native';

import {Actions} from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';

const dummy = [];

export default class SelectList extends Component {
    constructor(props) {
        super(props);
        console.log('props: ', props);
        this.state = {
            values: this.props.values,
            dataSource: ds.cloneWithRows(this.props.values ? this.props.values : dummy),
            visible: this.props.visible ? this.props.visible : false,
            value: this.props.value,
            placeholder: this.props.placeholder ? this.props.placeholder : Texts.SearchHere,
            search: '',
            highlight: {}
        };

        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
    }

    // componentWillReceiveProps(nextProps) {
    //     console.log('componentWillReceiveProps: ', nextProps);
        // this.setState({
        //     visible: nextProps.visible,
        //     text: nextProps.text,
        //     value: nextProps.value
        // })
    // }

    render() {
        return (
            <View style={[styles.container, this.state.visible ? { width: Values.width, height: Values.height} : { width: 0, height: 0 }]}>
                <View style={[styles.pickUpListContainer]}>
                    <View style={[styles.pickUpListHead, { }]}>
                        <View style={[styles.pickUpListHeader]}>
                            <View style={[styles.pickUpListClose, this.state.visible ? { width: 40 } : { width: 0 }]}>
                                <MaterialIcons onPress={this.hide} name="arrow-back" size={20} />
                            </View>
                            <View style={[styles.pickUpListTitle, this.state.highlight, this.state.visible ? { width: Values.input.fullWidth - 4, borderBottomWidth: 0 } : { width: 0, borderBottomWidth: 0 }]}>
                                <TextInput
                                    style={[styles.input, this.state.visible ? { width: Values.input.fullWidth - 4, padding: 10 } : { width: 0, padding: 0 }]}
                                    value={this.state.search}
                                    placeholder={ this.state.placeholder }
                                    placeholderTextColor={Values.input.placeholderTextColor}
                                    onChangeText={(text) => this.updateDataSource(text) }
                                    onBlur={ () => this.onBlur() }
                                    onFocus={() => this.onFocus() }
                                    />

                            </View>
                        </View>
                    </View>
                    <View style={styles.pickUpListBody}>
                        <ListView
                            keyboardShouldPersistTaps={true}
                            enableEmptySections={true}
                            dataSource={this.state.dataSource}
                            renderRow={this.props.renderRow}
                            />
                    </View>
                </View>

            </View>
        );
    }

    updateDataSource(text) {
        // console.log('updateDataSource: ', text);
        var newValues = [];
        text = text.toLowerCase();
        for (var i = 0; i < this.state.values.length; i++) {
            var value = this.state.values[i];
            if (value['name'].toLowerCase().indexOf(text) >= 0) {
                newValues.push(value);
            }
        }

        this.setState({
            search: text,
            dataSource: ds.cloneWithRows(newValues)
        });
    }

    show() {
        this.setState({
            search: '',
            visible: true,
            dataSource: ds.cloneWithRows(this.state.values)
        });
    }

    hide() {
        Actions.pop();
        this.setState({
            visible: false,
            dataSource: ds.cloneWithRows(dummy)
        });
    }


    onFocus() {
        this.setState({
            highlight: {
                borderColor: Values.colors.blue,
                backgroundColor: Values.colors.white,
            }
        });
    }

    onBlur() {
        this.setState({
            highlight: {}
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 0,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    pickUpListContainer: {
        backgroundColor: Values.colors.background,
        flexDirection: 'column',
        flex: 1,
        paddingTop: Values.margin
    },
    pickUpListHead: {

    },
    pickUpListHeader: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: Values.input.borderColor,
    },
    pickUpListClose: {
        padding: 10
    },
    pickUpListTitle: {
        flex: 1,
        padding: 10,
        paddingLeft: 0,
        borderColor: Values.input.borderColor
    },
    pickUpListBody: {
        marginBottom: 10,
        flex: 1
    },
    input: {
        backgroundColor: Values.colors.background,
        color: Values.input.color,
        
        alignSelf: 'center',
        fontSize: Values.input.fontSize,
        marginTop: -10,
        marginBottom: -10,
        height: 40
    },
})


const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });