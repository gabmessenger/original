import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';


import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';

export default class Button extends Component {
    constructor(props) {
        super(props);
    }

     render() {
         return (
             <TouchableHighlight style={[styles.bottonStyle, this.props.bottonStyle]} onPress={this.props.onPress} underlayColor={Values.button.underlayColor}>
                 <Text style={[styles.buttonText, this.props.buttonText]}>{this.props.text}</Text>
             </TouchableHighlight>
         );     
     }    


}

const styles = StyleSheet.create({
    bottonStyle: {
        borderWidth: 1,
        borderRadius: 3,
        borderColor: Values.button.borderColor,
        backgroundColor: Values.button.backgroundColor,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        height: 44,
        marginTop: 5,
        marginBottom: 10
    },
    buttonText: {
        color: '#eeeeee',
        fontSize: 17
    }
})
