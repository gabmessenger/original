import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Alert,
    Platform,
    AsyncStorage
} from 'react-native';

import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";
import DeviceInfo from 'react-native-device-info';
import Contacts from 'react-native-contacts';

import SmsListener from 'react-native-android-sms-listener'

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';
import Cache from '../../common/cache';

import Button from '../common/button';
import Input from '../common/input';

exports.title = '<StatusBar>';



export default class OTPVerification extends Component {
    constructor(props) {
        super(props);
        this.timeout = 15;

        // console.log('OTPVerification: ', this.props);
        this.state = {
            android: Platform.OS === 'android' ? true : false,
            _id: this.props._id,
            code: this.props.code,
            phone: this.props.phone,
            otp: '',
            expires: 60,
            verified: false,
            source: this.props.source, //SMS or Email
            enableEmail: {
                width: 0,
                height: 0
            },
            sessionExpire: {
                width: 0,
                height: 0
            }
        }

        this.goBack = this.goBack.bind(this);
        this.verifyPress = this.verifyPress.bind(this);
        this.sendEmail = this.sendEmail.bind(this);

        this.ImportAllPhoneContacts = this.ImportAllPhoneContacts.bind(this);

        if (this.state._id && this.state.source == 'SMS') {
            this.getTimerCountDown();
        }

        if (Platform.OS === 'android') {
            var self = this;
            this.subscription = SmsListener.addListener(message => {
                // console.log('Surya --- message: ', message);
                let verificationCodeRegex = /Gab-Messenger OTP ([\d]{6})/

                if (verificationCodeRegex.test(message.body)) {
                    let verificationCode = message.body.match(verificationCodeRegex)[1]
                    self.setState({ otp: verificationCode });

                    //self.verifyPress();
                }
            });
        }

    }


    getTimerCountDown() {
        var self = this;
        this.interval = setInterval(() => {
            // console.log('interval', this.state.expires);
            var expires = this.state.expires - 1;
            if (expires < 0) {
                clearInterval(this.interval);

            } else {
                this.setState({ expires: this.state.expires - 1 });
                if (expires <= this.timeout) {
                    this.setState({
                        enableEmail: {}
                    });
                }
            }
        }, 1000);

        setTimeout(() => {
            this.setState({
                sessionExpire: {}
            });
        }, 1000);
    }

    // componentDidMount() {
    //     if (this.state._id) {
    //         // this.getTimerCountDown();
    //     }
    // }

    render() {
        return (
            <View style={Styles.fullContainer}>
                <View style={Styles.mainContainer}>
                    <View style={Styles.titleContainer}>
                        <View style={[Styles.titleContents, styles.titleContents]}>
                            <TouchableOpacity onPress={this.goBack} style={this.state.verified ? { width: 0, height: 0, opacity: 0 } : { flexDirection: 'row', height: 22 }}>
                                <MaterialIcons name='chevron-left' size={35} color={Values.colors.blue} style={Styles.backArrow} />
                                <Text style={{ color: Values.colors.blue, fontSize: 17, lineHeight: 26 }}>{Texts.Back}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.bodyContainer}>
                        <View style={Styles.bodyContents}>
                            <View style={this.state.verified ? { width: 0, height: 0, opacity: 0 } : {}}>
                                <Text style={[Styles.titleText, styles.title]}>{this.state.phone ? (this.state.code + ' ' + this.state.phone) : Texts.YourPhoneNumberTitle}</Text>
                                <Text style={Styles.label}>{'\n\n' + Texts.sentSMS + '\n'}</Text>

                                <Input placeholder={'123456'} value={this.state.otp} onChange={(text) => this.setState({ otp: text })} keyboardType={'phone-pad'} />

                                <View style={{ paddingTop: 20 }} />
                                <Button text={'Verify'} onPress={this.verifyPress} />


                                <Text style={[Styles.label, this.state.sessionExpire]}>{'\n'}{Texts.sessionExpire} {this.state.expires} {Texts.secs}.</Text>


                                <Text style={[Styles.instructions, styles.sendEmail, this.state.enableEmail]} onPress={this.sendEmail}>{Texts.sendEmail}</Text>

                                <Text style={[Styles.label, styles.readingSMS, this.state.android ? {} : { width: 0, height: 0 }]}>{'\n'}{Texts.ReadingSMS}</Text>
                            </View>
                            <View style={this.state.verified ? {} : { width: 0, height: 0, opacity: 0 }}>
                                <Text style={styles.verified}>{Texts.successfulOTP}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>

        );
    }

    goBack() {
        var self = this;
        Actions.SignIn({ _id: self.state._id, code: self.state.code, phone: self.state.phone });
    }

    sendEmail() {
        var self = this;
        Actions.Email({ _id: self.state._id, code: self.state.code, phone: self.state.phone });
    }

    verifyPress() {
        var self = this;
        console.log('verifyPress: ', self.state.otp)
        var duid = DeviceInfo.getUniqueID();
        Meteor.call('VerifyOTP', self.state.code, self.state.phone, self.state.otp, duid, function (error, result) {
            // console.log(error, result);
            if (Platform.OS === 'android') {
                self.subscription.remove();
            }

            if (error) {
                Alert.alert(
                    Texts.invalidCode
                );
            } else {
                self.setState({
                    verified: true
                });
                // if (result) {
                //Go to Profile Page
                // console.log('Verified')
                Meteor.loginWithPassword(self.state.code + self.state.phone, self.state.otp, function (error) {
                    // console.log('Meteor: ', error);
                    if (error) {
                        alert(Texts.ErrorLoggingIn);
                    } else {
                        window.GM.U = Meteor.user();
                        Values.setStorage(AsyncStorage, 'user', Meteor.user());
                        self.ImportAllPhoneContacts();
                    }
                });


                // } else {
                //     Alert.alert(
                //         Texts.invalidCode
                //     );
                // }
            }
        });

    }


    ImportAllPhoneContacts() {
        console.log('ImportAllPhoneContacts');
        var self = this;
        Contacts.getAll((err, phoneContacts) => {
            // phoneContacts = Cache._suryaContacts;
            console.log('phoneContacts: ', phoneContacts);
            if (err && err.type === 'permissionDenied') {
                alert('No Permission to Access Contacts');
            } else {
                var allPhoneContacts = [];
                for (var phoneContact of phoneContacts) {
                    var name = phoneContact.givenName;
                    if (phoneContact.middleName) {
                        name += ' ' + phoneContact.middleName;
                    }
                    if (phoneContact.familyName) {
                        name += ' ' + phoneContact.familyName;
                    }

                    for (var phoneNumber of phoneContact.phoneNumbers) {
                        allPhoneContacts.push({
                            n: name,
                            p: phoneNumber.number,
                            r: phoneContact.recordID
                        });
                    }
                }

                console.log('allPhoneContacts: ', allPhoneContacts);

                var duid = DeviceInfo.getUniqueID();
                Meteor.call('ImportAllContacts', duid, allPhoneContacts, function (error, result) {
                    if (error) {

                    } else {
                        Cache.getGroups(Meteor, AsyncStorage);
                        Cache.getFriends(Meteor, AsyncStorage);


                        setTimeout(() => {
                            Actions.Profile();
                        }, 3000);
                    }
                });

            }
        });
        Cache.getContactsFromPhone(Contacts, AsyncStorage);

    }
}

const styles = StyleSheet.create({
    titleContents: {
        alignItems: 'flex-start',
    },
    title: {
        color: Values.text.color,
        fontSize: 20
    },
    sendEmail: {
        textAlign: 'left',
        color: Values.colors.blue,
        fontWeight: '400',
        marginTop: 8
    },
    readingSMS: {
        color: Values.colors.orange,
        textAlign: 'center'
    },
    verified: {
        textAlign: 'center',
        fontSize: 18
    }
})