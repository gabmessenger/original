import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Alert
} from 'react-native';

import Meteor, { createContainer } from 'react-native-meteor';
import {Actions} from "react-native-router-flux";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';

import Button from '../common/button';
import Input from '../common/input';

exports.title = '<StatusBar>';

export default class Email extends Component {
    constructor(props) {
        super(props);

        this.state = {
            _id: this.props._id,
            code: this.props.code,
            phone: this.props.phone,
            email: 'surya.iiit@gmail.com'
        }

        this.goBack = this.goBack.bind(this);
        this.sendEmail = this.sendEmail.bind(this);
    }

    //<Text style={[Styles.titleText, styles.title]}>{this.state.email ? this.state.email : Texts.email}</Text>
    render() {
        return (
            <View style={Styles.fullContainer}>
                <View style={Styles.mainContainer}>
                    <View style={Styles.titleContainer}>
                        <View style={[Styles.titleContents, styles.titleContents]}>
                            <TouchableOpacity onPress={this.goBack } style={{ flexDirection: 'row', height: 22 }}>
                                <MaterialIcons name='chevron-left' size={35} color={Values.colors.blue} style={Styles.backArrow}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.bodyContainer}>
                        <View style={Styles.bodyContents}>
                            <Text style={Styles.label}>{Texts.enterEmail + '\n'}</Text>

                            <Input placeholder={Texts.email} value={this.state.email} onChange={(text) => this.setState({ email: text }) }/>

                            <Button text={Texts.send} onPress={this.sendEmail} />


                        </View>
                    </View>
                </View>
            </View>

        );
    }

    goBack() {
        var self = this;
        Actions.SignIn({ _id: self.state._id, code: self.state.code, phone: self.state.phone });

    }

    sendEmail() {
        var self = this;

        Meteor.call('sendEmail', self.state._id, self.state.email, function (error, result) {
            // console.log(error, result);
            if (error) {
                Alert.alert(
                    Texts.errorSendingEmail
                );
            } else {
                //Go to Profile Page
                 Actions.OTPVerification({  source: 'Email', _id: self.state._id, code: self.state.code, phone: self.state.phone });

            }
        });

    }
}

const styles = StyleSheet.create({
    titleContents: {
        alignItems: 'flex-start',
    }
})