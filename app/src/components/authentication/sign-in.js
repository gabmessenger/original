import React, {
  Component
} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Alert
} from 'react-native';

import { Actions } from "react-native-router-flux";

import Meteor, {  createContainer } from 'react-native-meteor';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DeviceInfo from 'react-native-device-info';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';
import SelectList from '../common/select-list';

export default class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: null,
      code: '',
      codeLabel: 'Country Code',
      codeText: '',
      codeVisible: false,
      text: 'Select country',
      phone: ''

    }

    this.signInPress = this.signInPress.bind(this);
    // this.renderCode = this.renderCode.bind(this);
    // this.selectCode = this.selectCode.bind(this);
    this.showSelectList = this.showSelectList.bind(this);
    this.countryCodeRenderRow = this.countryCodeRenderRow.bind(this);
    this.updateCode = this.updateCode.bind(this);
  }

  //<Text style={Styles.instructions}>{Texts.YourPhoneNumberConfirm}</Text>
  //placeholderTextColor
  render() {
    return (
      <View style={Styles.fullContainer}>
        <View style={Styles.mainContainer} >

          <View style={Styles.titleContainer} >
            <View style={Styles.titleContents} >
              <Text style={[Styles.titleText, styles.title]}> </Text>
              <TouchableOpacity disabled={!(this.state.code && this.state.phone)} underlayColor={Values.button.underlayColor} onPress={() => this.confirmation()} >
                <Text style={[styles.done, !this.state.phone ? { color: Values.colors.lightGray } : {}]} > {Texts.done} </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={Styles.bodyContainer} >
            <View style={Styles.bodyContents} >
              <Select label={this.state.codeLabel} value={this.state.code} text={this.state.codeText} values={Texts.codes} renderRow={this.renderCode} visible={this.state.codeVisible} selectList={this.showSelectList} />
              <Input label={'Phone Number'} placeholder={Texts.PhoneNumberPlaceholder} value={this.state.phone} onChange={(text) => this.setState({ phone: text })} keyboardType={'phone-pad'} />
            </View>
          </View>

        </View>

        <SelectList values={Values.codes} renderRow={this.countryCodeRenderRow} />
      </View>
    );
  }

  showSelectList() {
    // console.log('Texts: ', Texts.codes)
    Actions.SelectList({
      visible: true,
      values: Texts.codes,
      renderRow: this.countryCodeRenderRow,
      placeholder: Texts.SearchCountry
    });
  }


  updateCode(country) {
    // console.log('updateCode')
    this.setState({
      code: country.dial_code,
      codeText: country.name + "  [" + country.dial_code + "]",
    });
    Actions.pop();
  }

  countryCodeRenderRow(value, s, i, h) {
    var selected = (value.dial_code === this.state.code);
    return (
      <TouchableOpacity key={i} underlayColor="#eee" onPress={() => { this.updateCode(value); h(s, i); } } >
        <View style={Styles.pickUpListItem} >
          <Text style={Styles.pickUpListItemName} > {value.name} </Text>
          <Text style={Styles.pickUpListItemValue} > {value.dial_code} </Text>
          {selected ? < MaterialIcons name='check' size={26} color='#0c0' style={Styles.pickUpListItemSelected} /> : <Text style={Styles.pickUpListItemNotSelected}> </Text >}
        </View>
      </TouchableOpacity>);
  }

  confirmation() {
    // this.signInPress();
    // return;
    var phone = this.state.code + this.state.phone;
    var pattern = /^\+[1-9]{1}[0-9]{8,14}$/;
    if (pattern.test(phone)) {
      Alert.alert(
        Texts.NumberConfirmation,
        '\n' + this.state.code + ' ' + this.state.phone + '\n\n' + Texts.IsPhoneNumberCorrect, [{
          text: Texts.edit,
          onPress: () => { }
        }, {
          text: Texts.yes,
          onPress: () => {
            this.signInPress();
          }
        }]
      );
    } else {
      Alert.alert(Texts.InvalidPhoneNumber);
    }

  }

  // renderCode(value, s, i, h) {
  //     var self = this;
  //     //onChange={(text) => this.setState({ code: text }) }

  //     var selected = (value.dial_code === this.state.code);
  //     return (
  //         <TouchableHighlight key={i} underlayColor={Values.input.underlayColor} onPress={() => {
  //             self.selectCode(value);
  //             h(s, i);
  //         } }>
  //             <View style={Styles.pickUpListItem}>
  //                 <Text style={Styles.pickUpListItemName}>{value.name}</Text>
  //                 <Text style={Styles.pickUpListItemValue}>{value.dial_code}</Text>
  //                 {selected ? <MaterialIcons name='check' size={26} color='#0c0' style={Styles.pickUpListItemSelected} /> : <Text style={Styles.pickUpListItemNotSelected}> </Text>}
  //             </View>
  //         </TouchableHighlight>);
  // }

  // selectCode(value) {
  //     // console.log(this.state, 'selectCode: ', value);
  //     this.setState({
  //         code: value.dial_code,
  //         codeText: value.name + " [" + value.dial_code + "]",
  //         codeVisible: false
  //     });
  //     // console.log(this.state, 'selectCode: ', {
  //     //     code: value.dial_code,
  //     //     codeText: value.dial_code + " (" + value.name + ")",
  //     //     codeVisible: false
  //     // });
  //     return;
  //     // this.setState({
  //     //     values: this.props.values ? ds.cloneWithRows(this.props.values) : null,
  //     //     value: value.dial_code,
  //     //     label: value.dial_code + " (" + value.name + ")",
  //     //     visible: false
  //     // });
  //     // this.props.onChange(value.dial_code);
  //     // console.log('_pressRow: ', value, this.state.value, (value.dial_code === this.state.value));
  // }


  details(data) {
    // console.log('details: ', data);
  }

  signInPress() {
    var self = this;
    var duid = DeviceInfo.getUniqueID();

    console.log('signInPress....', self.state.code, self.state.phone);

    Meteor.call('SMS', self.state.code, self.state.phone, duid, function (error, result) {
      console.log(error, result);
      if (error) {
        alert('Something went wrong, please try again after sometime.')
      } else {
        self.setState({
          _id: result
        });
        Actions.OTPVerification({
          name: 'OTP',
          source: 'SMS',
          _id: result,
          code: self.state.code,
          phone: self.state.phone
        })
      }

    });

  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column'
  },

  titleContainer: {
    alignItems: 'center',
    borderColor: Values.heading.borderColor,
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingBottom: 8,
    marginLeft: -1 * Values.margin,
    marginRight: -1 * Values.margin
  },
  titleContents: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  title: {
    flex: 1,
    fontWeight: '400'
  },
  done: {
    alignSelf: 'flex-end',
    color: Values.colors.blue,
    fontSize: Values.heading.fontSize,
    paddingRight: 10,
    fontWeight: '500'
  },

  bodyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 100
  },
  bodyContents: {

  }
});
