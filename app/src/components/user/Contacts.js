import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    ListView,
    ScrollView,
    LayoutAnimation,
    ActionSheetIOS,
    AsyncStorage
} from 'react-native';

import shallowequal from 'shallowequal';
import ActionSheet from 'react-native-actionsheet';
import Drawer from 'react-native-drawer';
import DeviceInfo from 'react-native-device-info';
import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import Contacts from 'react-native-contacts';
import Menu, {
    MenuContext,
    MenuOptions,
    MenuOption,
    MenuTrigger
} from 'react-native-menu';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';
import Cache from '../../common/cache';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';


const iconSize = 20;

var InviteButtons = [
    Texts.Message,
    Texts.Email,
    Texts.close,
];
var DESTRUCTIVE_INDEX = InviteButtons.length - 1;
var CANCEL_INDEX = InviteButtons.length - 1;
var CustomLayoutAnimation = {
    duration: 30,
    create: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.scaleXY,
    },
    update: {
        type: LayoutAnimation.Types.easeInEaseOut,
    }
};

class Friends extends Component {
    _phoneContacts = null;
    _contacts = null;
    _mPhoneContacts = null;
    _mContacts = null;

    constructor(props) {
        super(props);
        this.con = null;

        this.state = {
            _: null,
            dropdownSelection: null,
            search: '',
            highlight: {},
            contacts: [],
            dataSource: ds.cloneWithRows([]),
            addFriendVisible: true,
            addGroupVisible: true
        }
        console.log('GM: ', window.GM)

        // this.getAllPhoneContacts();
        // this.getAllContacts();

        console.log(Platform);
        this.showInviteOptions = this.showInviteOptions.bind(this);
        this.renderRow = this.renderRow.bind(this);
        // this.showAddFriend = this.showAddFriend.bind(this);
        // this.hideAddFriend = this.hideAddFriend.bind(this);
        // this.showAddGroup = this.showAddGroup.bind(this);
        // this.hideAddGroup = this.hideAddGroup.bind(this);

        // this.StoreContacts = this.StoreContacts.bind(this);
        // this.StorePhoneContacts = this.StorePhoneContacts.bind(this);
        // this.SetContacts = this.SetContacts.bind(this);


        // this.refs.MenuContext.openMenu()

    }


    componentDidMount() {
        // this.loadStorage();


        setTimeout(() => {
            var friends = Values.getFriends();
            this.setState({
                _: Values.getValues(),
                contacts: friends,
                dataSource: ds.cloneWithRows(friends),
            });
        }, 100)

    }

    componentWillReceiveProps(nextProps) {
        console.log('Contacts: ', nextProps);
        var newContacts = nextProps.contacts;
        var updatedContacts = !shallowequal(this.state.contacts, newContacts);

        console.log('updated: ', updatedContacts)
        if (updatedContacts) {

            Cache.setFriends(newContacts, AsyncStorage);

            var friends = Values.getFriends();

            this.setState({
                _: Values.getValues(),
                contacts: friends,
                dataSource: ds.cloneWithRows(friends),
            });
        // } else if (updatedOverviews) {
        //     this.setState({
        //         overviews: nextProps.overviews,
        //         dataSource: ds.cloneWithRows(nextProps.overviews)
        //     })
        // } else if (updatedGroups) {
        //     Cache.setGroups(Meteor.userId(), nextProps.groups, AsyncStorage);
        //     this.setState({
        //         groups: nextProps.groups
        //     });
        }
    }

    addMenuSelection(value) {
        console.log('addMenuSelection: ', value);
        if (value == 'invite') {
            this.showInviteOptions();
        } else if (value == 'friend') {
            this.showAddFriend();
        } else if (value == 'group') {
            this.showAddGroup();
        }
        this.refs.MenuContext.closeMenu('add');
    }


    showInviteOptions() {
        this.ActionSheet.show();
        return;

        // ActionSheetIOS.showActionSheetWithOptions({
        //     options: BUTTONS,
        //     cancelButtonIndex: CANCEL_INDEX,
        //     destructiveButtonIndex: CANCEL_INDEX,
        // },
        //     (buttonIndex) => {
        //         console.log('buttonIndex: ', buttonIndex);
        //         // this.setState({ clicked: BUTTONS[buttonIndex] });
        //     });
    };


    invite(index) {
        console.log('invite: ', index);
        if (InviteButtons[index] == Texts.Message) {
            console.log('Send Message ... ');
            Actions.InviteMessage({});
        } else if (InviteButtons[index] == Texts.Email) {
            console.log('Send Email ... ');
            Actions.InviteEmail({});
        }
    }

    showAddFriend() {
        console.log('showAddFriend: ');
        Actions.AddFriend({

        });
    }


    showAddGroup() {
        console.log('showAddGrou: ');
        Actions.AddGroup({

        });
    }

    render() {
        return (
            <MenuContext style={{ flex: 1 }} ref="MenuContext">

                <View>
                    <View style={Styles.mainContainer}>
                        <View style={[styles.titleContainer]}>
                            <View style={styles.titleContents}>
                                <View style={styles.leftItem}>

                                </View>
                                <View style={[styles.titleItem]}>
                                    <Text style={[styles.titleText]}>{Texts.Contacts}</Text>
                                </View>
                                <View style={styles.rightItem}>
                                    <Menu name={"add"} onSelect={(value) => this.addMenuSelection(value)}>
                                        <MenuTrigger>
                                            <View>
                                                <MaterialIcons name='add' size={25} color={Values.colors.blue} style={Styles.backArrow} style={{ paddingRight: 10 }} />
                                            </View>
                                        </MenuTrigger>
                                        <MenuOptions>
                                            <MenuOption value={'invite'}>
                                                <View style={styles.addMenu}>
                                                    <MaterialIcons name="contacts" size={iconSize} />
                                                    <Text style={styles.addMenuText}>{Texts.invite}</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption value={'friend'}>
                                                <View style={styles.addMenu}>
                                                    <MaterialIcons name="perm-contact-calendar" size={iconSize} />
                                                    <Text style={styles.addMenuText}>{Texts.addFriend}</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption value={'group'}>
                                                <View style={styles.addMenu}>
                                                    <MaterialIcons name="people" size={iconSize} />
                                                    <Text style={styles.addMenuText}>{Texts.addGroup}</Text>
                                                </View>
                                            </MenuOption>
                                        </MenuOptions>
                                    </Menu>
                                </View>
                            </View>
                        </View>

                        <View style={[styles.bodyContainer, styles.contacts]}>
                            <View style={[styles.searchInputContainer, this.state.highlight]}>
                                <TextInput
                                    style={[styles.searchInput]}
                                    value={this.state.search}
                                    placeholder={Texts.Search}
                                    placeholderTextColor={Values.input.placeholderTextColor}
                                    onChangeText={(text) => this.updateDataSource(text)}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    onBlur={() => this.onBlur()}
                                    onFocus={() => this.onFocus()}
                                    />

                            </View>
                            <View style={[styles.contactsList]}>
                                <ListView
                                    dataSource={this.state.dataSource}
                                    renderRow={this.renderRow}
                                    enableEmptySections={true}
                                    />
                            </View>
                        </View>
                    </View>
                    <ActionSheet
                        ref={(o) => this.ActionSheet = o}
                        title={Texts.invite}
                        options={InviteButtons}
                        cancelButtonIndex={CANCEL_INDEX}
                        // destructiveButtonIndex={DESTRUCTIVE_INDEX}
                        onPress={this.invite.bind(this)}
                        />

                </View>
            </MenuContext>
        );
    }


    renderRow(friend, s, i, h) {
        console.log('friend: ', friend);
        var self = this;
        var contact = self.state._ && self.state._[friend] ? self.state._[friend] : { n: '?' };

        return (
            <View style={styles.contact}>
                <View style={styles.details}>
                    <Text style={styles.name}>{contact.n}</Text>
                </View>
            </View>
        );

    }

    updateDataSource(text) {
        console.log('updateDataSource: ', text);
        var newValues = [];
        var searhInput = text;
        text = text.toLowerCase();
        for (var i = 0; i < this.state.contacts.length; i++) {
            var contact = this.state.contacts[i];
            // console.log(contact, ':updateDataSource: ', text);
            if (contact.givenName.toLowerCase().indexOf(text) >= 0
                || (contact.familyName && contact.familyName.toLowerCase().indexOf(text) >= 0)
                || (contact.middleName && contact.middleName.toLowerCase().indexOf(text) >= 0)) {
                newValues.push(contact);
            }
        }

        this.setState({
            search: searhInput,
            dataSource: ds.cloneWithRows(newValues)
        });
    }

    onFocus() {
        this.setState({
            highlight: {
                borderColor: Values.colors.gray,
                backgroundColor: Values.colors.white,
                borderTopWidth: 1
            }
        });
    }

    onBlur() {
        this.setState({
            highlight: {

            }
        });
    }


    getAllContacts() {
        console.log('getAllContacts')
        var self = this;
        Meteor.call('GetAllContacts', function (error, contacts) {
            console.log('GetAllContacts: ', contacts, error);
            if (error) {
                Alert.alert(
                    Texts.ErrorGettingGroup
                );
            } else {
                console.log('contacts: ', contacts);
                self._contacts = contacts;
                if (contacts && contacts.length > 0) {
                    self.StoreContacts(contacts);
                } else {
                    self.setState({
                        contacts: []
                    });
                }
                self.CheckImports();
            }
        });
    }

    StoreContacts(contacts) {
        var contactsMap = [];
        var _mContacts = {};
        for (let contact of contacts) {
            if (contact._F) {
                contactsMap.push([contact._F, contact.p]);
                _mContacts[contact._F] = contact.p;
            }
        };
        this._mContacts = _mContacts;
        this.SetContacts();

        console.log('contactsMap: ', contactsMap);
        // Values.setStorageFriends(AsyncStorage, _mContacts);

    }


    SetContacts() {
        var self = this;
        var contacts = [];
        console.log('SetContacts: ', self._mContacts, self._mPhoneContacts);
        if (self._mContacts && self._mPhoneContacts) {
            for (let contactId in self._mContacts) {
                var id = contactId;
                var phone = self._mContacts[id];
                var details = self._mPhoneContacts[phone];
                contacts.push({
                    n: details.n,
                    p: phone,
                    i: id
                })
            }
        }
        console.log('contacts: ', contacts);

        self.setState({
            contacts: contacts,
            dataSource: ds.cloneWithRows(contacts)
        })
    }

    // getAllPhoneContacts() {
    //     console.log('getAllPhoneContacts: ', Meteor.user())
    //     var self = this;
    //     Contacts.getAll((err, phoneContacts) => {
    //         console.log('phoneContacts: ', phoneContacts);
    //         if (err && err.type === 'permissionDenied') {
    //             alert('No Permission to Access Contacts');
    //         } else {
    //             self._phoneContacts = phoneContacts;
    //             // this.StorePhoneContacts();
    //             self.CheckImports();

    //         }
    //     });
    // }


    CheckImports() {
        console.log('CheckImports')
        var self = this;
        if (self._contacts && self._phoneContacts) {
            if (self._contacts.length == 0) {
                //Import All Contacts
            } else {
                self.CheckForNewContacts();
            }
        }
    }

    //TODO    
    CheckForNewContacts() {
        console.log('CheckForNewContacts')
    }

    StorePhoneContacts() {
        var self = this;
        let user = Meteor.user();

        const userCode = user.profile.code;
        const userPhone = user.profile.phone;

        var phoneNameMap = [];
        var _mPhoneContacts = {};

        for (var phoneContact of self._phoneContacts) {
            var name = phoneContact.givenName;
            if (phoneContact.middleName) {
                name += ' ' + phoneContact.middleName;
            }
            if (phoneContact.familyName) {
                name += ' ' + phoneContact.familyName;
            }

            for (var phoneNumber of phoneContact.phoneNumbers) {

                var phone = phoneNumber.number;
                phone = phone.replace(/[^0-9]/g, '').replace(/^0+/g, '');
                if (phone.length == userPhone.length) {
                    phone = userCode + phone;
                } else if (phone.length == userCode.length + userPhone.length) {
                    //Nothing already country code is there.
                } else {

                }

                var value = { n: name, p: phoneNumber.number, r: phoneContact.r };
                phoneNameMap.push([
                    phone, JSON.stringify(value)
                ]);

                _mPhoneContacts[phone] = value;
            }
        }
        this._mPhoneContacts = _mPhoneContacts;
        this.SetContacts();

        console.log('phoneNameMap: ', phoneNameMap);
        AsyncStorage.multiSet(phoneNameMap, (err) => {
            console.log('multiSet: ', err);
        });
    }
}

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const actionsHeight = 60;
const borderWidth = 2;

const drawerStyles = {
    drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3, position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, height: 300, backgroundColor: '#0f0' },
    main: {},
}

const styles = StyleSheet.create({
    bodyContainer: {
        flex: 1,
        // marginLeft: -1 * Values.margin,
        // marginRight: -1 * Values.margin,
        backgroundColor: Values.colors.white
    },
    bodyContents: {

    },

    contacts: {
        height: Values.height,
        // borderTopWidth: 2,
        // borderColor: Values.colors.lightGray,
        marginTop: -borderWidth,
    },
    contact: {
        borderColor: Values.heading.borderColor,
        borderBottomWidth: 1,
        flex: 1,
        // flexDirection: 'row',
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        paddingTop: 5,
        paddingBottom: 5,
        // height: 65
    },
    contactsList: {
        height: Values.height - 160
    },
    plusMenu: {
        marginTop: 5,
        marginRight: 14
    },
    // pic: {
    //     width: 44,
    //     height: 44,
    //     borderRadius: 22,
    //     marginTop: 5
    // },
    details: {
        // flex: 1,
        // paddingLeft: 10,
        height: 25
    },
    name: {
        fontSize: 15,
        lineHeight: 25,
        // fontWeight: '600'
    },
    phone: {
        fontSize: 16
    },
    actions: {
        flex: 1,
        flexDirection: 'row',
        height: actionsHeight,
        backgroundColor: Values.colors.lightGray,
        opacity: 0.62
    },
    addMenu: {
        flex: 1,
        flexDirection: 'row',
    },
    addMenuText: {
        paddingLeft: 8
    },

    searchInputContainer: {
        borderColor: Values.input.borderColor,
        borderWidth: 0,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        // borderRadius: 3,
        height: 38,
        padding: 1,
    },
    searchInput: {
        // backgroundColor: Values.input.backgroundColor,
        color: Values.input.color,
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        alignSelf: 'center',
        height: 35,
        fontSize: Values.input.fontSize,
        width: Values.width,
        textAlign: 'left'
    },

    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: Platform.select({
            ios: 5,
            android: -5,
        }),
        height: 60,
        paddingBottom: 5
    },

    titleContents: {
        flex: 1,
        flexDirection: 'column',
        marginTop: Values.margin
    },
    leftItem: {
        position: 'absolute',
        left: 2,
        width: 100,
        // textAlign: 'left'
    },
    leftText: {
        // textAlign: 'left'
    },
    titleItem: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0
    },
    titleText: {
        textAlign: 'center',
        fontSize: 18,
        width: Values.width - 200 > 180 ? Values.width - 200 : 180,
        alignSelf: 'center',
    },
    rightItem: {
        position: 'absolute',
        right: 2,
        width: 100,
        alignItems: 'flex-end'
    },
    rightText: {
        // 
    },
});



export default createContainer(params => {
    Meteor.subscribe('Contacts');

    var userId = Meteor.userId();    

    return {
        contacts: Meteor.collection('contacts').find({ "_U": userId })
    }
}, Friends);