import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ListView,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    Alert
} from 'react-native';


import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Contacts from 'react-native-contacts';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

import Menu from './Menu';


const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default class InviteMessage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownSelection: null,
            search: '',
            highlight: {},
            contacts: [],
            selected: [],
            dataSource: ds.cloneWithRows([]),
            sending: false
        };

        this.goBack = this.goBack.bind(this);
        this.send = this.send.bind(this);
        this.renderRow = this.renderRow.bind(this);
        this.selectContact = this.selectContact.bind(this);

        this.getAllContacts();
    }


    goBack() {
        Actions.Contacts();
    }

    send() {
        var self = this;
        var phoneNumbers = [];
        for (var i = 0; i < this.state.contacts.length; i++) {
            var contact = this.state.contacts[i];
            if (this.state.selected.indexOf(contact.recordID) >= 0 && contact.phoneNumbers && contact.phoneNumbers.length > 0) {
                for (var j = 0; j < contact.phoneNumbers.length; j++) {
                    var phoneNumber = contact.phoneNumbers[j].number;
                    phoneNumbers.push(phoneNumber.replace(/[^0-9]/g, '').replace(/^0+/g, ''));
                }
            }
        }

        console.log('phoneNumbers: ', phoneNumbers);
        message = 'Message from gab-messenger';

        Meteor.call('InviteMessage', phoneNumbers, message, function(error, result) {
            console.log('InviteEmail: ', error);
            if (error) {
                Alert.alert(
                    Texts.errorSendingMessage
                );
                self.setState({
                    sending: false
                });
            } else {
                self.goBack();
            }
        });
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.goBack} style={styles.closeButton}>
                            <Text style={[styles.cancelAction]}>{Texts.cancel}</Text>
                        </TouchableOpacity>
                        <Text style={[Styles.titleText, styles.title]}>{Texts.Message}</Text>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.send} disabled={this.state.sending} style={[styles.closeButton]}>
                            <Text style={[styles.sendAction, this.state.sending ? { color: Values.colors.lightGray } : {}]}>{Texts.send}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.bodyContainer}>
                    <View style={[styles.bodyContents]}>
                        <View style={[styles.searchInputContainer, this.state.highlight]}>
                            <TextInput
                                style={[styles.searchInput]}
                                value={this.state.search}
                                placeholder={Texts.Search}
                                placeholderTextColor={Values.input.placeholderTextColor}
                                onChangeText={(text) => this.updateDataSource(text)}
                                onBlur={() => this.onBlur()}
                                onFocus={() => this.onFocus()}
                                />

                        </View>

                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow}
                            enableEmptySections={true}
                            />
                    </View>
                </View>
            </View>
        );
    }


    updateDataSource(text) {
        console.log('updateDataSource: ', text);
        var newValues = [];
        var searhInput = text;
        text = text.toLowerCase();
        for (var i = 0; i < this.state.contacts.length; i++) {
            var contact = this.state.contacts[i];
            // console.log(contact, ':updateDataSource: ', text);
            if (contact.givenName.toLowerCase().indexOf(text) >= 0
                || (contact.familyName && contact.familyName.toLowerCase().indexOf(text) >= 0)
                || (contact.middleName && contact.middleName.toLowerCase().indexOf(text) >= 0)) {
                newValues.push(contact);
            }
        }

        this.setState({
            search: searhInput,
            dataSource: ds.cloneWithRows(newValues)
        });
    }

    onFocus() {
        this.setState({
            highlight: {
                borderColor: Values.colors.gray,
                backgroundColor: Values.colors.white,
                borderTopWidth: 1
            }
        });
    }

    onBlur() {
        this.setState({
            highlight: {

            }
        });
    }

    selectContact(contact) {
        var selected = this.state.selected;
        console.log(selected, ' - selectContact: ', contact);

        if (selected.indexOf(contact.recordID) >= 0) {
            selected.splice(selected.indexOf(contact.recordID), 1);
        } else {
            selected.push(contact.recordID);
        }
        console.log('selected: ', selected);
        this.setState({
            selected: selected
        })
    }

    renderRow(contact, s, i, h) {
        var self = this;
        var selected = this.state.selected.indexOf(contact.recordID) >= 0 ? true : false;
        return (
            <TouchableOpacity style={styles.contact} onPress={() => { this.selectContact(contact); h(s, i); } }>
                <View style={styles.details}>
                    <MaterialIcons name={selected ? "check-box" : "check-box-outline-blank"} size={24} style={selected ? styles.selected : styles.select} />
                    <Text style={styles.name}>{contact.givenName} {contact.familyName}</Text>
                </View>
            </TouchableOpacity>
        );

    }


    getAllContacts() {
        Contacts.getAll((err, contacts) => {
            console.log('contacts: ', contacts);
            if (err && err.type === 'permissionDenied') {
                alert('Please allow Contacts')
            } else {
                this.setState({
                    contacts: contacts,
                    dataSource: ds.cloneWithRows(contacts)
                });
            }
        })
    }
}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        // borderColor: Values.colors.lightGray,
        // borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 5,
        height: 55,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height
    },
    bodyContents: {
    },
    cancelAction: {
        color: Values.colors.blue,
        paddingLeft: 5
    },
    sendAction: {
        color: Values.colors.blue,
        paddingRight: 5,
        fontWeight: 'bold'
    },
    contacts: {
        height: Values.height,
        // borderTopWidth: 2,
        // borderColor: Values.colors.lightGray,
        marginTop: -2
    },
    contact: {
        borderColor: Values.heading.borderColor,
        borderBottomWidth: 1,
        flex: 1,
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        paddingTop: 5,
        paddingBottom: 5,
        // height: 65
    },
    plusMenu: {
        marginTop: 5,
        marginRight: 14
    },
    details: {
        height: 25,
        flexDirection: 'row'
    },
    name: {
        fontSize: 15,
        lineHeight: 25,
    },
    searchInputContainer: {
        borderColor: Values.input.borderColor,
        borderWidth: 0,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 38,
        padding: 1,
    },
    searchInput: {
        // backgroundColor: Values.input.backgroundColor,
        color: Values.input.color,
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        alignSelf: 'center',
        height: 35,
        fontSize: Values.input.fontSize,
        width: Values.width,
        textAlign: 'left'
    },
    select: {
        opacity: 0.5,
        paddingRight: 8,
        color: Values.colors.blue
    },
    selected: {
        paddingRight: 8,
        color: Values.colors.blue
    }
});


