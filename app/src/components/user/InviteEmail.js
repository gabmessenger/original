import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    Alert
} from 'react-native';


import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

import Menu from './Menu';

export default class InviteEmail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            to: '',
            subject: '',
            content: '',
            sending: false
        };
        this.goBack = this.goBack.bind(this);
        this.send = this.send.bind(this);
    }


    goBack() {
        Actions.Contacts();
    }

    send() {
        console.log('sending...')
        var self = this;
        self.setState({
            sending: true
        });
        Meteor.call('InviteEmail', self.state.to, self.state.subject, self.state.content, function (error, result) {
            console.log('InviteEmail: ', error);
            if (error) {
                Alert.alert(
                    Texts.errorSendingEmail
                );
                self.setState({
                    sending: false
                });
            } else {
                self.goBack();
            }
        });
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.goBack} style={styles.closeButton}>
                            <Text style={[styles.cancelAction]}>{Texts.cancel}</Text>
                        </TouchableOpacity>
                        <Text style={[Styles.titleText, styles.title]}>{Texts.Email}</Text>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.send} disabled={this.state.sending} style={[styles.closeButton]}>
                            <Text style={[styles.sendAction, this.state.sending ? { color: Values.colors.lightGray } : {}]}>{Texts.send}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.bodyContainer}>
                    <View style={[styles.bodyContents]}>
                        <Input label={Texts.To} placeholder={Texts.To} value={this.state.to} onChange={(text) => this.setState({ to: text })} />
                        <Input label={Texts.Subject} placeholder={Texts.Subject} value={this.state.subject} onChange={(text) => this.setState({ subject: text })} />
                        <Input inputStyle={styles.content} label={Texts.Content} placeholder={Texts.Content} value={this.state.content} onChange={(text) => this.setState({ content: text })} multiline={true} />
                    </View>
                </View>
            </View>
        );
    }


}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 5,
        height: 55,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height
    },
    bodyContents: {
    },
    cancelAction: {
        color: Values.colors.blue,
        paddingLeft: 8
    },
    sendAction: {
        color: Values.colors.blue,
        paddingRight: 8,
        fontWeight: 'bold'
    },
    content: {
        height: Values.height - 2 * 100,
        // backgroundColor: Values.,
        // borderColor: '#CCCCCC',
        // borderWidth: 1,
    }
});


