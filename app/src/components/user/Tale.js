import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    WebView,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform
} from 'react-native';


import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

export default class Tale extends Component {
    _id;

    constructor(props) {
        super(props);
        console.log('-');
        console.log('Tale props: ', this.props.id);
        console.log('-');
        this.state = {
            id: this.props.id,
            tale: null
        }
        this.goBack = this.goBack.bind(this);
        this.getTale = this.getTale.bind(this);


    }

    componentWillReceiveProps(nextProps) {
        console.log('Tale componentWillReceiveProps', this.state.id)
        if (this.state.id != nextProps.id) {
            console.log('nextProps: ', nextProps.id);
            this.setState({
                id: nextProps.id
            });
            this._id = nextProps.id;
            console.log(this._id, 'this.state.id: ', this.state.id);
            this.getTale();
        }
    }


    goBack() {
        Actions.Tales();
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <TouchableOpacity onPress={this.goBack} style={{ flexDirection: 'row', height: 22 }}>
                            <MaterialIcons name='chevron-left' size={35} color={Values.colors.blue} style={Styles.backArrow} />
                            <Text style={{ color: Values.colors.blue, fontSize: 17, lineHeight: 26 }}>{Texts.Back}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.bodyContainer}>
                    <View style={[styles.bodyContents]}>
                        <WebView
                            style={styles.webView}
                            source={{ uri: this.state.tale ? this.state.tale.l : 'https://www.gabmessenger.chat/' }}
                            />
                    </View>
                </View>
            </View>
        );
    }

    getTale() {
        var self = this;
        console.log(this._id, 'Get Tale: ', self.state.id);
        Meteor.call('GetTale', this._id, function (error, tale) {
            console.log('Tale: ', tale, error);
            if (error) {
                Alert.alert(
                    Texts.ErrorGettingTale
                );
            } else {
                self.setState({
                    tale: tale
                });
            }
        });
    }

}


const styles = StyleSheet.create({
    webView: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        height: Values.height - 64,
        width: Values.width
    },
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 5,
        height: 55,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height
    },
    bodyContents: {
    },
    cancelAction: {
        color: Values.colors.blue,
        paddingLeft: 5
    },
    doneAction: {
        color: Values.colors.blue,
        paddingRight: 8,
        fontWeight: 'bold'
    },
});


