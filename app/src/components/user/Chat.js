import React from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    AsyncStorage
} from 'react-native';


import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';
import Cache from '../../common/cache';

import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { GiftedChat, Bubble } from '../chat/GiftedChat';
import CustomActions from './CustomActions';
import CustomView from './CustomView';

class Chat extends React.Component {

    _G;

    constructor(props) {
        super(props);

        console.log(';;');
        console.log('window._', window._);
        this.state = {
            title: Texts.Chat,
            G: null,
            F: null,
            user: Meteor.user(),
            contact: null,
            messages: props.messages,
            loadEarlier: false,
            typingText: null,
            isLoadingEarlier: false,
        };

        this.goBack = this.goBack.bind(this);

        this._isMounted = false;
        this.onSend = this.onSend.bind(this);
        this.onReceive = this.onReceive.bind(this);
        this.renderCustomActions = this.renderCustomActions.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
        this.renderFooter = this.renderFooter.bind(this);
        this.onLoadEarlier = this.onLoadEarlier.bind(this);

        this.GetInternalGroup = this.GetInternalGroup.bind(this);
        this._isAlright = null;

    }

    componentDidMount() {
        this.setState({
            _: Values.getValues()
        });
    }



    componentWillReceiveProps(nextProps) {
        console.log(this.state.G, nextProps.group, 'Chat componentWillReceiveProps', nextProps, this.state.contact, !this.state.contact);
        // if (!this.state.contact && nextProps.contact) {
        //     // console.log('nextProps: ');
        //     // console.log('nextProps: ', nextProps.contact);

        //     var contact = nextProps.contact;

        //     var messages = nextProps.messages;
        //     var U = window.GM.U;
        //     this.setState({
        //         contact: contact,
        //         friend: this.state._[contact],
        //         F: U._id,
        //         messages: messages
        //     });

        //     console.log(this.state._[contact], 'this.state.contact: ', this.state.contact);
        //     var self = this;
        //     setTimeout(() => {
        //         console.log('this.state.contact 2: ', self.state.contact);
        //         self.GetInternalGroup();
        //     }, 1000);

        //     console.log('this.state.messages: ', this.state.messages);
        // } else if (this.state.contact && nextProps.messages) {
        //     var messages = nextProps.messages;
        //     console.log('messages: ', messages);
        //     this.setState({
        //         messages: messages
        //     });
        // } else
        if ((!this.state.G && nextProps.group) || this.state.G != nextProps.group) {
            var self = this;
            var U = window.GM.U;
            console.log(U, 'Chat Group', nextProps.group);
            this.setState({
                F: U._id,
                G: nextProps.group,
                messages: nextProps.messages //Meteor.collection('chats').find({ _G: nextProps.group }, { sort: { a: -1 } })
            });

            self._G = nextProps.group;

            // setTimeout(() => {
            self.GetGroupDetails();
            // }, 200);

        } else {
            this.setState({
                messages: nextProps.messages
            });
        }
    }

    GetInternalGroup() {
        var self = this;
        console.log('GetInternalGroup: ', self.state.contact);
        Meteor.call('GetInternalGroup', self.state.contact, function (error, result) {
            console.log('GetInternalGroup: ', error, result);
            if (error) {

            } else {
                self.setState({
                    G: result
                })
            }
        });
    }

    GetGroupDetails() {
        var _ = Values.getValues();
        // var G = window.GM.G;
        // var D = window.GM.D;

        console.log('_: ', _);
        // console.log('G: ', G);
        // console.log('D: ', D);

        var name = '';
        var group = _[this._G];
        if (!group._ && group.i) { //Group & Internal Group
            this.setState({
                title: _[group.U] ? _[group.U].n : ''
            });
        } else {
            name = group.n;
            this.setState({
                title: group.n
            });
        }

        var contactName = '';
        if (!group._ && group.i) {
            //contactName = _[overview._F].n;
        }


        // var self = this;
        // console.log('GetGroupDetails: ', self.state.G);
        // Meteor.call('GetGroupDetails', self.state.G, function (error, contact) {
        //     console.log('GetGroupDetails: ', error, contact);
        //     if (error) {

        //     } else {
        //         self.setState({
        //             contact: contact,
        //             friend: self.state._[contact],
        //             F: contact,
        //             messages: Meteor.collection('chats').find({_G: self.state.G}, { sort: { a: -1 } })
        //         })
        //     }
        // });
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }


    goBack() {
        Actions.Chats();
    }


    onLoadEarlier() {
        this.setState((previousState) => {
            return {
                isLoadingEarlier: true,
            };
        });

        setTimeout(() => {
            if (this._isMounted === true) {
                this.setState((previousState) => {
                    return {
                        messages: GiftedChat.prepend(previousState.messages, [
                            {
                                _id: Math.round(Math.random() * 1000000),
                                text: 'It uses the same design as React, letting you compose a rich mobile UI from declarative components https://facebook.github.io/react-native/',
                                createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
                                user: {
                                    _id: 1,
                                    name: 'Developer',
                                },
                            },
                            {
                                _id: Math.round(Math.random() * 1000000),
                                text: 'React Native lets you build mobile apps using only JavaScript',
                                createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
                                user: {
                                    _id: 1,
                                    name: 'Developer',
                                },
                            },
                        ]),
                        // loadEarlier: false,
                        isLoadingEarlier: false,
                    };
                });
            }
        }, 1000); // simulating network
    }

    onSend(messages = []) {
        var self = this;
        // console.log('onSend: ', messages);
        var message = messages[0].text;
        Meteor.call('SendMessage', self.state.G, self.state.contact, message, null, function(error, result) {
            console.log('SendMessage: ', result, error);
            if (error) {
                Alert.alert(
                    Texts.ErrorSendingChat
                );
            } else {

            }
        });

        // this.setState((previousState) => {
        //     return {
        //         messages: GiftedChat.append(previousState.messages, messages),
        //     };
        // });

        // // for demo purpose
        // this.answerDemo(messages);
    }

    answerDemo(messages) {
        if (messages.length > 0) {
            if ((messages[0].image || messages[0].location) || !this._isAlright) {
                this.setState((previousState) => {
                    return {
                        typingText: 'React Native is typing'
                    };
                });
            }
        }

        setTimeout(() => {
            if (this._isMounted === true) {
                if (messages.length > 0) {
                    if (messages[0].image) {
                        this.onReceive('Nice picture!');
                    } else if (messages[0].location) {
                        this.onReceive('My favorite place');
                    } else {
                        if (!this._isAlright) {
                            this._isAlright = true;
                            this.onReceive('Alright');
                        }
                    }
                }
            }

            this.setState((previousState) => {
                return {
                    typingText: null,
                };
            });
        }, 1000);
    }

    onReceive(text) {
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, {
                    _id: Math.round(Math.random() * 1000000),
                    text: text,
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'React Native',
                        // avatar: 'https://facebook.github.io/react/img/logo_og.png',
                    },
                }),
            };
        });
    }

    renderCustomActions(props) {
        if (Platform.OS === 'ios') {
            return (
                <CustomActions
                    {...props}
                    />
            );
        }
        //To Handle Android Actions
        const options = {
            'Action 1': (props) => {
                alert('option 1');
            },
            'Action 2': (props) => {
                alert('option 2');
            },
            'Cancel': () => { },
        };
        return null;
    }

    renderBubble(props) {
        return (
            <Bubble
                {...props}
                wrapperStyle={{

                }}
                />
        );
    }

    renderCustomView(props) {
        return (
            <CustomView
                {...props}
                />
        );
    }

    renderFooter(props) {
        if (this.state.typingText) {
            return (
                <View style={styles.footerContainer}>
                    <Text style={styles.footerText}>
                        {this.state.typingText}
                    </Text>
                </View>
            );
        }
        return null;
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <View style={styles.leftItem}>
                            <TouchableOpacity onPress={this.goBack} style={[styles.leftText, { flexDirection: 'row', height: 22 }]}>
                                <MaterialIcons name='chevron-left' size={35} color={Values.colors.blue} style={Styles.backArrow} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.titleItem, { flexDirection: 'row' }]}>
                            {this.state.friend && this.state.friend.tp
                                ? <Image defaultSource={require('../../images/profile_default.png')} style={styles.contactPic} source={this.state.contact.tp} />
                                : <Image defaultSource={require('../../images/profile_default.png')} style={styles.contactPic} />
                            }
                            <Text style={[styles.titleText]} numberOfLines={1}>{this.state.title}</Text>
                        </View>
                        <View style={styles.rightItem}>

                        </View>
                    </View>
                </View>
                <View style={[styles.bodyContainer]}>
                    <GiftedChat
                        messages={this.state.messages}
                        onSend={this.onSend}
                        loadEarlier={this.state.loadEarlier}
                        onLoadEarlier={this.onLoadEarlier}
                        isLoadingEarlier={this.state.isLoadingEarlier}
                        user={{
                            _F: this.state.F, // sent messages should have same user._id
                        }}
                        renderActions={this.renderCustomActions}
                        renderBubble={this.renderBubble}
                        renderCustomView={this.renderCustomView}
                        renderFooter={this.renderFooter}
                        />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: Platform.select({
            ios: 5,
            android: -5,
        }),
        height: 60,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'column',
        marginTop: Values.margin
    },
    leftItem: {
        position: 'absolute',
        left: 2,
        width: 30,
        // textAlign: 'left'
    },
    leftText: {
        // textAlign: 'left'
    },
    titleItem: {
        position: 'absolute',
        left: 40,
        right: 100,
        top: 0
    },
    titleText: {
        // textAlign: 'center',
        fontSize: 18,
        width: Values.width - 100 > 180 ? Values.width - 100 : 180,
        // alignSelf: 'center',
    },
    rightItem: {
        position: 'absolute',
        right: 2,
        width: 100,
        alignItems: 'flex-end'
    },
    rightText: {
        // 
    },

    contactPic: {
        borderRadius: 15,
        width: 30,
        height: 30,
        marginLeft: 4,
        marginRight: 8,
        marginTop: -4
    },

    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height - 60,
        marginTop: 1,
        width: Values.width
    },
    footerContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    footerText: {
        fontSize: 14,
        color: '#aaa',
    },
});


export default createContainer(params => {
    console.log('')
    console.log('')
    console.log('params: ', params);

    Meteor.subscribe('Chats');

    return {
        messages: Meteor.collection('chats').find({ _G: params.group }, { sort: { a: -1 } })
    }
}, Chat);