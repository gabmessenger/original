import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    AsyncStorage
} from 'react-native';

import { Actions } from "react-native-router-flux";
import Meteor, { createContainer } from 'react-native-meteor';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

import Menu from './Menu';

class Chats extends Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    render() {
        return (
            <View style={Styles.fullContainer}>
                <View style={Styles.mainContainer}>
                    <View style={Styles.titleContainer}>
                        <View style={Styles.titleContents}>
                            <Text style={[Styles.titleText, styles.title]}>{Texts.Settings}</Text>
                        </View>
                    </View>
                    <View style={Styles.bodyContainer}>
                        <View style={Styles.bodyContents}>
                            <Text style={[Styles.titleText, styles.title]}>{Texts.Settings}</Text>
                            <Button text={Texts.logout} onPress={this.logout} />
                        </View>
                    </View>
                </View>
            </View>

        );
    }

    logout() {
        console.log('logout')
        Meteor.logout();
         Values.removeStorage(AsyncStorage, 'user').then(function (err, res) { 
            console.log(err, res)
        });
         Values.removeStorage(AsyncStorage, 'details').then(function (err, res) { 
            console.log(err, res)
         });
         Values.removeStorage(AsyncStorage, 'groups').then(function (err, res) { 
            console.log(err, res)
         });
         Values.removeStorage(AsyncStorage, 'phoneFriends').then(function (err, res) { 
            console.log(err, res)
         });
         Values.removeStorage(AsyncStorage, 'friendPhones').then(function (err, res) { 
            console.log(err, res)
         });
         Values.removeStorage(AsyncStorage, 'nonFriends').then(function (err, res) { 
            console.log(err, res)
         });

        window.GM = {};

        console.log('logout 2')
        
        Actions.Intro();
    }

}


const styles = StyleSheet.create({

});



export default createContainer(params => {
    return {
        connected: Meteor.status().connected,
        user: Meteor.user()
    }
}, Chats);