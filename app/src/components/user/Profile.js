import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    ScrollView
} from 'react-native';

import { Actions } from "react-native-router-flux";
import ImagePicker from 'react-native-image-picker';
import { RNS3 } from 'react-native-aws3';

import Meteor, { createContainer } from 'react-native-meteor';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

let s3Options = {
    keyPrefix: "chats/",
    bucket: "gab-messenger-chat",
    region: Values.AWS.S3.region,
    accessKey: Values.AWS.S3.AccessKey,
    secretKey: Values.AWS.S3.SecretKey,
    successActionStatus: 201
}

var options = {
    title: 'Select Avatar',
    customButtons: [
        { name: 'fb', title: 'Choose Photo from Facebook' },
    ],
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

class Profile extends Component {
    constructor(props) {
        super(props);
        console.log('Profile: ', props);
        var user = this.props.user;
        this.state = {
            user: this.props.user,
            name: user && user.profile ? user.profile.name : null,
            email: user && (user.emails && user.emails.length > 0) ? user.emails[0].address : null,
            phone: user ? user.username : null,
            pic: user && (user.profile && user.profile.pic) ? { uri: user.profile.pic } : null
        };

        this.done = this.done.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        console.log('Profile componentWillReceiveProps: ', nextProps);
        // alert('componentWillReceiveProps: ' + nextProps);
    }

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        var self = this;
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                var source;

                // You can display the image using either:
                //source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

                //Or:
                if (Platform.OS === 'android') {
                    source = { uri: response.uri, isStatic: true };
                } else {
                    source = { uri: response.uri.replace('file://', ''), isStatic: true };
                }

                console.log('source: ', source);

                this.setState({
                    pic: source
                });

                let file = {
                    // `uri` can also be a file system path (i.e. file://) 
                    uri: source.uri,
                    name: self.state.user._id + "-" + new Date().getTime() + ".jpg",
                    type: "image/jpg"
                };

                console.log('put');
                RNS3.put(file, s3Options).then(response => {
                    if (response.status !== 201)
                        throw new Error("Failed to upload image to S3");
                    console.log(response);

                    var pic = response.body.postResponse.location;
                    Meteor.call('SaveProfilePic', pic, function (error, result) {
                        console.log(error, result);
                        if (error) {
                            alert(error);
                        } else {

                        }
                    });
                }).catch(error => {
                    console.log('error: ', error);
                    alert(Texts.profilePicUploadError)
                }).progress((e) => console.log(e.loaded * 100 / e.total, e.loaded, e.total));

                console.log('kept');
            }
        });
    }

    render() {
        const { connected, user } = this.props;
        // console.log('Profile render: ', connected, user);

        return (
            <View style={styles.mainContainer}>
                <View style={styles.titleContainer}>
                    <View style={styles.titleContents}>
                        <View style={styles.leftItem}>

                        </View>
                        <View style={[styles.titleItem]}>
                            <Text style={[styles.titleText]}>{Texts.profile}</Text>
                        </View>
                        <View style={styles.rightItem}>
                            <TouchableOpacity disabled={!this.state.phone} underlayColor={Values.button.underlayColor} onPress={() => this.done()}>
                                <Text style={[styles.done, !this.state.phone ? { color: Values.colors.lightGray } : {}]}>{Texts.done}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={Styles.bodyContainer}>
                    <ScrollView style={[Styles.bodyContents, styles.bodyContents]}>
                        <TouchableOpacity style={styles.userPic} onPress={this.selectPhotoTapped.bind(this)}>
                            <View style={[Styles.profilePicContainer, Styles.profilePic]}>
                                {this.state.pic === null ? <Text>Select a Photo</Text> :
                                    <Image defaultSource={require('../../images/profile_default.png')} style={Styles.profilePic} source={this.state.pic} />
                                }
                            </View>
                        </TouchableOpacity>

                        <Input label={'Name'} placeholder={'Firstname Lastname'} value={this.state.name} onChange={(text) => this.setState({ name: text })} />
                        <Input label={'Email (optional)'} placeholder={'email@gab-messenger.chat'} value={this.state.email} onChange={(text) => this.setState({ email: text })} />
                        <Input label={'Phone'} value={this.state.phone} />

                    </ScrollView>
                </View>
            </View >

        );
    }

    done() {
        var self = this;
        Meteor.call('SaveProfile', self.state.name, self.state.email, function (error, result) {
            if (error) {
                alert(Texts.profileSaveError)
            } else {
                Actions.Contacts();
            }
        });
    }

}


const styles = StyleSheet.create({
   
    userPic: {
        alignItems: 'center'
    },

    done: {
        color: Values.colors.blue,
        fontSize: Values.heading.fontSize,
        fontWeight: '500'
    },

    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 1.25 * Values.padding,
        height: 50,
        paddingBottom: Values.padding,
        marginBottom: Values.margin,
    },
    titleContents: {
        flex: 1,
        flexDirection: 'column'
    },
    leftItem: {
        position: 'absolute',
        left: 2,
        width: 100,
        // textAlign: 'left'
    },
    leftText: {
        // textAlign: 'left'
    },
    titleItem: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0
    },
    titleText: {
        textAlign: 'center',
        fontSize: 18,
        width: Values.width - 200 > 180 ? Values.width - 200 : 180,
        alignSelf: 'center',
    },
    rightItem: {
        position: 'absolute',
        right: 2,
        width: 100,
        alignItems: 'flex-end'
    },
    rightText: {
        // 
    },

});



export default createContainer(params => {
    // console.log('profile container');
    return {
        connected: Meteor.status().connected,
        user: Meteor.user()
    }
}, Profile);