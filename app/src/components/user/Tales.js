import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ListView,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform
} from 'react-native';


import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

import Menu from './Menu';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class News extends Component {

    constructor(props) {
        super(props);
        console.log(props);

        this.state = {
            tales: [],
            dataSource: ds.cloneWithRows([]),
        };

        this.renderRow = this.renderRow.bind(this);
        this.showTale = this.showTale.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log('nextProps: ', nextProps);
        this.setState({
            tales: nextProps.tales,
            dataSource: ds.cloneWithRows(nextProps.tales)
        });
    }


    showTale(news) {
        console.log('showTale: ', news._id);
        Actions.Tale({
            // id: news._id
        });


        setTimeout(() => {
            Actions.refresh({ id: news._id })
        }, 1000);

    }


    render() {
        return (
            <View style={styles.mainContainer} >
                
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <View style={styles.leftItem}>

                        </View>
                        <View style={[styles.titleItem]}>
                            <Text style={[styles.titleText]}>{Texts.Tales}</Text>
                        </View>
                        <View style={styles.rightItem}>

                        </View>
                    </View>
                </View>

                <View style={[styles.bodyContainer]}>
                    <View style={[styles.newsList]}>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow}
                            enableEmptySections={true}
                            />
                    </View>
                </View>

            </View>
        );
    }

    renderRow(news, s, i, h) {
        return (
            <TouchableOpacity style={styles.news} onPress={() => { this.showTale(news); h(s, i); } }>
                <View style={styles.newsTitle}>
                    <Text style={styles.title}>{news.t}{news.t}</Text>
                </View>
                <View style={styles.newsBrief}>
                    <Text style={styles.brief}>{news.b}</Text>
                </View>
            </TouchableOpacity>
        );
    }



}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 5,
        height: 60,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'column',
        marginTop: Values.margin
    },
    leftItem: {
        position: 'absolute',
        left: 2,
        width: 100,
        // textAlign: 'left'
    },
    leftText: {
        // textAlign: 'left'
    },
    titleItem: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0
    },
    titleText: {
        textAlign: 'center',
        fontSize: 18,
        width: Values.width - 200 > 180 ? Values.width - 200 : 180,
        alignSelf: 'center',
    },
    rightItem: {
        position: 'absolute',
        right: 2,
        width: 100,
        alignItems: 'flex-end'
    },
    rightText: {
        // 
    },

    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height - 120,
        marginTop: 1,
        width: Values.width
    },
    
    newsList: {
        height: Values.height - 120,
    },
    news: {
        borderColor: Values.heading.borderColor,
        borderBottomWidth: 1,
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        paddingTop: 10,
        paddingBottom: 10,
    },
    newsTitle: {

    },
    title: {
        color: Values.colors.black,
        fontSize: 17,
        marginBottom: 5
    },
    newsBrief: {

    },
    brief: {
        fontSize: 16,
        color: Values.colors.darkGray,
    }
});



export default createContainer(params => {
    Meteor.subscribe('TopTales');

    return {
        tales: Meteor.collection('tales').find()
    }
}, News);

