import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ListView,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    AsyncStorage
} from 'react-native';

import shallowequal from 'shallowequal';
import DeviceInfo from 'react-native-device-info';
import Meteor, { createContainer } from 'react-native-meteor';
import moment from 'moment/min/moment-with-locales.min';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Actions } from "react-native-router-flux";

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';
import Cache from '../../common/cache';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

import Menu from './Menu';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const messages = [];

class Chats extends Component {
    constructor(props) {
        super(props);
        console.log('window.GM: ', window.GM);
        this.state = {
            _: null,
            groups: window.GM.g,
            contacts: [],
            overviews: props.overviews ? props.overviews : [],
            dataSource: ds.cloneWithRows(props.overviews ? props.overviews : [])
        }

        this.renderChat = this.renderChat.bind(this);

        this.newChat = this.newChat.bind(this);
        this.startChat = this.startChat.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        console.log('Chats: ', nextProps);
        var newOverviews = nextProps.overviews;
        var newGroups = nextProps.groups;
        var newContacts = nextProps.contacts;

        var updatedOverviews = !shallowequal(this.state.overviews, newOverviews);
        var updatedGroups = !shallowequal(this.state.groups, newGroups);
        var updatedContacts = !shallowequal(this.state.contacts, newContacts);

        console.log('updated: ', updatedOverviews, updatedGroups, updatedContacts)
        if (updatedOverviews || updatedGroups || updatedContacts) {

            Cache.setFriends(newContacts, AsyncStorage);
            Cache.setGroups(Meteor.userId(), newGroups, AsyncStorage);

            this.setState({
                _: Values.getValues(),
                contacts: newContacts,
                groups: newGroups,
                overviews: newOverviews,
                dataSource: ds.cloneWithRows(newOverviews)
            });
            // } else if (updatedOverviews) {
            //     this.setState({
            //         overviews: nextProps.overviews,
            //         dataSource: ds.cloneWithRows(nextProps.overviews)
            //     })
            // } else if (updatedGroups) {
            //     Cache.setGroups(Meteor.userId(), nextProps.groups, AsyncStorage);
            //     this.setState({
            //         groups: nextProps.groups
            //     });
        }
    }

    componentDidMount() {
        this.setState({
            _: Values.getValues(),
        });
    }



    newChat() {
        Actions.NewChat({});
    }

    render() {
        return (
            <View style={styles.mainContainer} >
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <View style={styles.leftItem}>

                        </View>
                        <View style={[styles.titleItem]}>
                            <Text style={[styles.titleText]}>{Texts.Chats}</Text>
                        </View>
                        <View style={styles.rightItem}>
                            <TouchableOpacity onPress={this.newChat} style={[styles.rightText, { flexDirection: 'row', height: 22, marginRight: 14, marginTop: 6 }]}>
                                <MaterialIcons name='message' size={26} color={Values.colors.blue} style={Styles.backArrow} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={[styles.bodyContainer]}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderChat}
                        enableEmptySections={true}
                        />
                </View>
            </View>
        );
    }

    renderChat(overview, s, i, h) {
        var _ = this.state._;
        console.log('___: ', _)
        var U = window.GM.U;

        console.log(_, 'overview: ', overview);
        var name = '';
        var contactName = '';
        var group = _[overview._G];
        if (group) {
            if (!group._ && group.i && _[group.U]) {    //Group & Internal Group
                name = _[group.U].n;
            } else {
                name = group.n;
            }

            if (!group._ && !group.i) {
                contactName = _[overview._F] ? _[overview._F].n : '??';
            }
        }




        return (
            <TouchableOpacity style={styles.overview} onPress={() => { this.startChat(overview); h(s, i); } }>
                <View style={styles.overviewIcon}>
                    <Image defaultSource={require('../../images/group-default.png')} style={styles.overviewPic} />
                </View>
                <View style={styles.overviewDetails}>
                    <View style={styles.overviewMore}>
                        <Text style={styles.overviewName} numberOfLines={1}>{name}</Text>
                        <Text style={styles.overviewTime}>{moment(overview.a).locale('en').format('LT')}</Text>
                    </View>
                    <Text style={styles.overviewFrom}>{contactName} </Text>
                    <Text style={styles.overviewText}>{overview.m}</Text>
                </View>
                <View style={styles.overviewGo}>
                    <MaterialIcons name='chevron-right' size={26} color={Values.colors.gray} />
                </View>
            </TouchableOpacity>
        );
    }

    startChat(overview) {
        console.log('startChat: ', overview);
        Actions.Chat({
            group: overview._G
        });


        setTimeout(() => {
            Actions.refresh({ group: overview._G })
        }, 10);
    }

    sendMessage() {
        console.log('Message: ', this.state.message);
        this.setState({ message: '' });
    }

}


const styles = StyleSheet.create({
    overview: {
        flexDirection: 'row',
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10
    },
    overviewPic: {
        width: 50,
        height: 50,
        marginLeft: 10,
        marginRight: 10
    },
    overviewGo: {
        width: 30,
        opacity: 0.7,
    },
    overviewDetails: {
        flexDirection: 'column',
        flex: 1
    },
    overviewText: {
        opacity: 0.7
    },

    overviewMore: {
        flexDirection: 'row',
    },
    overviewName: {
        fontWeight: 'bold',
        fontSize: 15,
        flex: 1
    },
    overviewTime: {
        textAlign: 'right',
        opacity: 0.8
    },


    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: Platform.select({
            ios: 5,
            android: -5,
        }),
        height: 60,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'column',
        marginTop: Values.margin
    },
    leftItem: {
        position: 'absolute',
        left: 2,
        width: 100,
        // textAlign: 'left'
    },
    leftText: {
        // textAlign: 'left'
    },
    titleItem: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0
    },
    titleText: {
        textAlign: 'center',
        fontSize: 18,
        width: Values.width - 200 > 180 ? Values.width - 200 : 180,
        alignSelf: 'center',
    },
    rightItem: {
        position: 'absolute',
        right: 2,
        width: 100,
        alignItems: 'flex-end'
    },
    rightText: {
        // 
    },

    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height - 120,
        marginTop: 1,
        width: Values.width
    },
});



export default createContainer(params => {
    Meteor.subscribe('Overviews');
    Meteor.subscribe('Groups');
    Meteor.subscribe('Contacts');

    var groupIds = Values.getGroupIds();
    var userId = Meteor.userId();
    console.log('userId: ', userId);
    console.log('groupIds: ', groupIds);
    return {
        contacts: Meteor.collection('contacts').find({ "_U": userId }),
        groups: Meteor.collection('groups').find({ "_U": userId }),
        overviews: Meteor.collection('overviews').find({ _G: { $in: groupIds }, m: { $exists: true } }, { sort: { a: -1 } })
    }

}, Chats);