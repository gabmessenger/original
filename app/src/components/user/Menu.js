import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform
} from 'react-native';

import {Actions} from "react-native-router-flux";
import Meteor, { createContainer } from 'react-native-meteor';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';


import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';



export default class Menu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.menuContainer}>
                <View style={[styles.itemContainer, this.props.active == Texts.Chats ? styles.activeItemContainer : {}]}>
                    <TouchableOpacity  onPress={() => this.goToChats() }>
                        <MaterialIcons name='chat' size={30} color={Values.colors.blue} style={[styles.menuItem, { marginTop: 5 }]}/>
                    </TouchableOpacity>
                </View>
                <View style={[styles.itemContainer, this.props.active == Texts.News ? styles.activeItemContainer : {}]}>
                    <TouchableOpacity  onPress={() => this.goToNews() }>
                        <MaterialIcons name='web' size={35} color={Values.colors.blue} style={styles.menuItem}/>
                    </TouchableOpacity>
                </View>
                <View style={[styles.itemContainer, this.props.active == Texts.Contacts ? styles.activeItemContainer : {}]}>
                    <TouchableOpacity  onPress={() => this.goToContacts() }>
                        <MaterialIcons name='people' size={35} color={Values.colors.blue} style={styles.menuItem}/>
                    </TouchableOpacity>
                </View>
                <View style={[styles.itemContainer, this.props.active == Texts.Settings ? styles.activeItemContainer : {}]}>
                    <TouchableOpacity  onPress={() => this.goToSettings() }>
                        <MaterialIcons name='settings' size={28} color={Values.colors.blue} style={[styles.menuItem, { marginTop: 4 }]}/>
                    </TouchableOpacity>
                </View>
                <View style={[styles.itemContainer, this.props.active == Texts.Chats ? styles.activeItemContainer : {}]}>
                    <MaterialIcons name='more-horiz' size={35} color={Values.colors.blue} style={styles.menuItem}/>
                </View>
            </View>

        );
    }


    goToChats() {
        Actions.Chats();
    }

    goToContacts() {
        Actions.Contacts();
    }

    goToNews() {
        Actions.News();
    }

    goToSettings() {
        Actions.Settings();
    }

}


const styles = StyleSheet.create({
    menuContainer: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: -1 * Values.margin,
        marginRight: -1 * Values.margin,
        borderColor: Values.colors.gray,
        backgroundColor: Values.colors.lightBackground,
        borderTopWidth: 1 / PixelRatio.get(),
        height: 60
    },
    itemContainer: {
        flex: 1,
        height: 50,
        paddingTop: 10,
        paddingBottom: 8,
        alignItems: 'center',
        opacity: 0.62
    },
    menuItem: {

    },
    activeItemContainer: {
        opacity: 1
    }
});

