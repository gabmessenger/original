import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ListView,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    AsyncStorage
} from 'react-native';


import Meteor, { createContainer } from 'react-native-meteor';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Actions } from "react-native-router-flux";

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';
import Cache from '../../common/cache';

import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class NewChat extends Component {
    _phone;
    _contact;

    constructor(props) {
        super(props);
        console.log('props: ', props);
        this.state = {
            _: null,
            contacts: [],
            dataSource: ds.cloneWithRows([])
        }

        this.goBack = this.goBack.bind(this);
        // this.SetContacts = this.SetContacts.bind(this);

        this.renderContact = this.renderContact.bind(this);
        this.startChat = this.startChat.bind(this);


    }

    componentDidMount() {
        setTimeout(() => {
            var friends = Values.getFriends();
            this.setState({
                _: Values.getValues(),
                contacts: friends,
                dataSource: ds.cloneWithRows(friends),
            });
        }, 100)
    }


    goBack() {
        Actions.Chats();
    }


    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <View style={styles.leftItem}>
                            <TouchableOpacity onPress={this.goBack} style={[styles.leftText, { flexDirection: 'row', height: 22 }]}>
                                <Text style={{ color: Values.colors.blue, fontSize: 17, lineHeight: 26 }}>{Texts.cancel}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.titleItem]}>
                            <Text style={[styles.titleText]}>{Texts.NewChat}</Text>
                        </View>
                        <View style={styles.rightItem}>

                        </View>
                    </View>
                </View>
                <View style={[styles.bodyContainer]}>
                    <View style={[styles.chatsList]}>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderContact}
                            enableEmptySections={true}
                            />
                    </View>
                </View>
            </View>
        );
    }


    renderContact(contact, s, i, h) {
        var self = this;
        // console.log(self.state._, contact, self.state._[contact]);
        return (
            <TouchableOpacity onPress={() => { this.startChat(contact); h(s, i); } }>
                <View style={styles.contact}>
                    <View style={styles.details}>
                        <Text style={styles.name}>{self.state._[contact] ? self.state._[contact].n : ''}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );

    }

    startChat(contact) {
        console.log('startChat: ', contact);
        var self = this;
        Meteor.call('GetInternalGroup', contact, function (error, result) {
            console.log('GetInternalGroup: ', error, result);
            if (error) {

            } else {
               
                Actions.Chat({
                    contact: contact,
                    group: result
                });


                setTimeout(() => {
                    Actions.refresh({ contact: contact, group: result })
                }, 10);
            }
        });

    }


    sendMessage() {
        console.log('Message: ', this.state.message);
        this.setState({ message: '' });
    }


}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 5,
        height: 55,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'column',
        marginTop: Values.margin
    },
    leftItem: {
        position: 'absolute',
        left: 2,
        width: 100,
        // textAlign: 'left'
    },
    leftText: {
        // textAlign: 'left'
    },
    titleItem: {
        position: 'absolute',
        left: 100,
        right: 100,
        top: 0,
    },
    titleText: {
        textAlign: 'center',
        fontSize: 18,
        width: Values.width - 200 > 180 ? Values.width - 200 : 180,
        alignSelf: 'center',
    },
    rightItem: {
        position: 'absolute',
        right: 2,
        width: 100,
        alignItems: 'flex-end'
    },
    rightText: {
        // 
    },

    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height
    },

    chatsList: {
        height: Values.height - 123,
    },
    newChat: {
        padding: Values.padding,
        height: 60,
        width: Values.width,
        borderTopWidth: 1,
        borderColor: '#f3f3f3',
        backgroundColor: '#fff'
    },

    contact: {
        borderColor: Values.heading.borderColor,
        borderBottomWidth: 1,
        flex: 1,
        // flexDirection: 'row',
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        paddingTop: 5,
        paddingBottom: 5,
        // height: 65
    },

    details: {
        // flex: 1,
        // paddingLeft: 10,
        height: 25
    },
    name: {
        fontSize: 15,
        lineHeight: 25,
        // fontWeight: '600'
    },
});



export default createContainer(params => {
    return {

    }
}, NewChat);