import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ListView,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    AsyncStorage
} from 'react-native';


import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Contacts from 'react-native-contacts';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';
import Cache from '../../common/cache';

import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

import Menu from './Menu';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
export default class AddGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            groupName: '',

            search: '',
            highlight: {},
            contacts: [],
            selected: [],
            dataSource: ds.cloneWithRows([]),

            max: 256,
            saving: false
        }
        this.goBack = this.goBack.bind(this);
        this.save = this.save.bind(this);
        this.renderRow = this.renderRow.bind(this);
        this.selectContact = this.selectContact.bind(this);


    }

    componentDidMount() {
        setTimeout(() => {
            var friends = Values.getFriends();
            this.setState({
                _: Values.getValues(),
                contacts: friends,
                dataSource: ds.cloneWithRows(friends),
            });
        }, 100)
    }

    componentWillReceiveProps(nextProps) {
        console.log('AddGroup', nextProps)
    }


    goBack() {
        Actions.Contacts();
    }

    save() {
        var users = this.state.selected;
        var self = this;
        self.setState({
            saving: true
        });
        Meteor.call('SaveGroup', self.state.groupName, users, null, function (error, result) {
            console.log('SaveGroup: ', error);
            if (error) {
                Alert.alert(
                    Texts.ErrorSavingGroup
                );
                self.setState({
                    saving: false
                });
            } else {
                //Go To that group

                Cache.getGroups(Meteor, AsyncStorage);

            }
        });
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.goBack} style={styles.closeButton}>
                            <Text style={[styles.cancelAction]}>{Texts.cancel}</Text>
                        </TouchableOpacity>
                        <Text style={[Styles.titleText, styles.title]}>{Texts.NewGroup}</Text>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.save} disabled={this.state.saving} style={[styles.closeButton]}>
                            <Text style={[styles.doneAction, this.state.saving ? { color: Values.colors.lightGray } : {}]}>{Texts.done}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.bodyContainer}>
                    <View style={[styles.bodyContents]}>
                        <View style={{ height: 40 }}>
                            <Input label={Texts.GroupName} placeholder={Texts.GroupName} value={this.state.groupName} onChange={(text) => this.setState({ groupName: text })} />
                        </View>
                        <View style={{ height: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>{Texts.Participants} {this.state.selected.length}of {this.state.max}</Text>
                        </View>
                        <View style={[styles.searchInputContainer, this.state.highlight]}>
                            <TextInput
                                style={[styles.searchInput]}
                                value={this.state.search}
                                placeholder={Texts.Search}
                                placeholderTextColor={Values.input.placeholderTextColor}
                                onChangeText={(text) => this.updateDataSource(text)}
                                onBlur={() => this.onBlur()}
                                onFocus={() => this.onFocus()}
                                />

                        </View>

                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow}
                            enableEmptySections={true}
                            />

                    </View>
                </View>
            </View>
        );
    }

    renderRow(contact, s, i, h) {
       
        var self = this;
        var selected = this.state.selected.indexOf(contact) >= 0 ? true : false;
        return (
            <TouchableOpacity style={styles.contact} onPress={() => { this.selectContact(contact); h(s, i); } }>
                <View style={styles.details}>
                    <Text style={styles.name}>{self.state._[contact] ? self.state._[contact].n : ''}</Text>
                    <MaterialIcons name={selected ? "check" : "add-circle"} size={24} style={selected ? styles.selected : styles.select} />
                </View>
            </TouchableOpacity>
        );

    }


    updateDataSource(text) {
        console.log('updateDataSource: ', text);
        var newValues = [];
        var searhInput = text;
        text = text.toLowerCase();
        for (var i = 0; i < this.state.contacts.length; i++) {
            var contact = this.state.contacts[i];
            // console.log(contact, ':updateDataSource: ', text);
            if (contact.givenName.toLowerCase().indexOf(text) >= 0
                || (contact.familyName && contact.familyName.toLowerCase().indexOf(text) >= 0)
                || (contact.middleName && contact.middleName.toLowerCase().indexOf(text) >= 0)) {
                newValues.push(contact);
            }
        }

        this.setState({
            search: searhInput,
            dataSource: ds.cloneWithRows(newValues)
        });
    }

    onFocus() {
        this.setState({
            highlight: {
                borderColor: Values.colors.gray,
                backgroundColor: Values.colors.white,
                borderTopWidth: 1
            }
        });
    }

    onBlur() {
        this.setState({
            highlight: {

            }
        });
    }

    selectContact(contact) {
        var selected = this.state.selected;
        console.log(selected, ' - selectContact: ', contact);

        if (selected.indexOf(contact) >= 0) {
            selected.splice(selected.indexOf(contact), 1);
        } else {
            if (selected.length < this.state.max) {
                selected.push(contact);
            }
        }
        console.log('selected: ', selected);
        this.setState({
            selected: selected
        })
    }

    getAllContacts() {
        var self = this;
        Meteor.call('GetAllContacts', function (error, contacts) {
            console.log('GetAllContacts: ', error);
            if (error) {
                Alert.alert(
                    Texts.ErrorGettingGroup
                );
            } else {
                self.setState({
                    contacts: contacts,
                    dataSource: ds.cloneWithRows(contacts)
                });

            }
        });
    }

}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 5,
        height: 55,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height
    },
    bodyContents: {
        flex: 1
    },
    cancelAction: {
        color: Values.colors.blue,
        paddingLeft: 5
    },
    doneAction: {
        color: Values.colors.blue,
        paddingRight: 8,
        fontWeight: 'bold'
    },
    contacts: {
        height: Values.height,
        // borderTopWidth: 2,
        // borderColor: Values.colors.lightGray,
        marginTop: -2
    },
    contact: {
        borderColor: Values.colors.lightestGray,
        borderBottomWidth: 1,
        flex: 1,
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        paddingTop: 5,
        paddingBottom: 5,
        // height: 65
    },
    plusMenu: {
        marginTop: 5,
        marginRight: 14
    },
    details: {
        height: 25,
        flexDirection: 'row'
    },
    name: {
        fontSize: 15,
        lineHeight: 25,
        flex: 1
    },
    searchInputContainer: {
        borderColor: Values.input.borderColor,
        borderWidth: 0,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 38,
        padding: 1,
    },
    searchInput: {
        // backgroundColor: Values.input.backgroundColor,
        color: Values.input.color,
        paddingLeft: Values.margin,
        paddingRight: Values.margin,
        alignSelf: 'center',
        height: 35,
        fontSize: Values.input.fontSize,
        width: Values.width,
        textAlign: 'left'
    },
    select: {
        paddingLeft: 8,
        color: Values.colors.green
    },
    selected: {
        paddingLeft: 8,
        color: Values.colors.blue
    }
});


