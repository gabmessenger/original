import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    PixelRatio,
    Image,
    Platform,
    Alert,
    AsyncStorage
} from 'react-native';


import Meteor, { createContainer } from 'react-native-meteor';
import { Actions } from "react-native-router-flux";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Contacts from 'react-native-contacts';

import Styles from '../../common/styles';
import Values from '../../common/values';
import Texts from '../../common/texts';
import Cache from '../../common/cache';

import Button from '../common/button';
import Input from '../common/input';
import Select from '../common/select';

import Menu from './Menu';

export default class AddFriend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            saving: false
        }
        this.goBack = this.goBack.bind(this);
        this.save = this.save.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log('AddFriend', nextProps)
    }


    goBack(item) {
        Actions.Contacts();
    }

    save() {
        var self = this;
        Meteor.call('AddFriend', this.state.phoneNumber, function (error, result) {
            console.log('GetAAddFriendllContacts: ', error, result);
            if (error) {
                Alert.alert(
                    Texts.ErrorSavingContact
                );
            } else {
                Cache.getContactsFromPhone(Contacts, AsyncStorage);
                Cache.getGroups(Meteor, AsyncStorage);
                Cache.getFriends(Meteor, AsyncStorage);

                self.goBack();
            }
        });

        var contact = {
            familyName: this.state.firstName,
            givenName: this.state.lastName,
            middleName: "",
            phoneNumbers: [{
                label: "mobile",
                number: this.state.phoneNumber,
            }]
        };


        Contacts.addContact(contact, (data) => {
            console.log('addContact: ', data);
        });


    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={[styles.titleContainer]}>
                    <View style={styles.titleContents}>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.goBack} style={styles.closeButton}>
                            <Text style={[styles.cancelAction]}>{Texts.cancel}</Text>
                        </TouchableOpacity>
                        <Text style={[Styles.titleText, styles.title]}>{Texts.NewContact}</Text>
                        <TouchableOpacity underlayColor={Values.button.underlayColor} onPress={this.save} disabled={this.state.saving} style={[styles.closeButton]}>
                            <Text style={[styles.doneAction, this.state.saving ? { color: Values.colors.lightGray } : {}]}>{Texts.done}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.bodyContainer}>
                    <View style={[styles.bodyContents]}>
                        <Input label={Texts.FirstName} placeholder={Texts.FirstName} value={this.state.firstName} onChange={(text) => this.setState({ firstName: text })} />
                        <Input label={Texts.LastName} placeholder={Texts.LastName} value={this.state.lastName} onChange={(text) => this.setState({ lastName: text })} />
                        <Input label={Texts.PhoneNumber} placeholder={Texts.PhoneNumber} value={this.state.phoneNumber} onChange={(text) => this.setState({ phoneNumber: text })} />
                    </View>
                </View>
            </View>
        );
    }

}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        height: Values.height
    },
    titleContainer: {
        borderColor: Values.colors.lightGray,
        borderBottomWidth: 1,
        backgroundColor: Values.colors.background,
        paddingTop: 5,
        height: 55,
        paddingBottom: 5
    },
    titleContents: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    bodyContainer: {
        backgroundColor: Values.colors.white,
        height: Values.height
    },
    bodyContents: {
    },
    cancelAction: {
        color: Values.colors.blue,
        paddingLeft: 5
    },
    doneAction: {
        color: Values.colors.blue,
        paddingRight: 8,
        fontWeight: 'bold'
    },
});


