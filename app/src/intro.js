import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TextInput,
    Image
} from 'react-native';

import {Actions} from "react-native-router-flux";

import Swiper from 'react-native-swiper';

import Button from './components/common/button';

import Styles from './common/styles';
import Values from './common/values';
import texts from './common/texts';

export default class Intro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Surya'
        }
        this.signInPress = this.signInPress.bind(this);
        
        
    }

    render() {
        return (
            <ScrollView style={[Styles.viewContainer, { flexDirection: 'column' }]}>
                <View style={styles.slides}>
                    <Swiper style={styles.wrapper} showsButtons={false} autoplay={true} autoplayTimeout={5}
                    dot={<View style={{backgroundColor: Values.colors.gray, width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                    activeDot={<View style={{backgroundColor: Values.colors.blue, width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />} 
                    paginationStyle={{bottom: Values.height/6 }}>
                        <View style={styles.slide1}>
                            <Text style={styles.text}>{texts.welcome1}</Text>
                        </View>
                        <View style={styles.slide2}>
                            <Text style={styles.text}>{texts.welcome2}</Text>
                        </View>
                        <View style={styles.slide3}>
                            <Text style={styles.text}>{texts.welcome3}</Text>
                        </View>
                    </Swiper>
                </View>
                <View style={styles.signin}>
                    
                    <Button text='Sign In' onPress={this.signInPress}/>
                </View>
            </ScrollView>
        );
    }

    signInPress() {
        Actions.SignIn();
        // this.props.navigator.push({ name: 'SignIn', _: 1 });
    }
}

const styles = StyleSheet.create({
    slides: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: Values.height - 140
    },
    signin: {
        height: 140,
        // backgroundColor: '#f00',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    text: {
        color: '#333',
        fontSize: 26,
        lineHeight: 38,
        // fontWeight: 'bold',
        textAlign: 'center',
        margin: 20
    }
});
