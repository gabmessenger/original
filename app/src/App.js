import React, { Component } from 'react';
import {
    Navigator,
    StyleSheet,
    Layout,
    Image,
    View,
    Text,
    AsyncStorage
} from 'react-native';

import { Scene, Router, TabBar, Modal, Schema, Actions, Reducer, ActionConst, Switch } from 'react-native-router-flux';

import Meteor, { createContainer } from 'react-native-meteor';

import SelectList from './components/common/select-list';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Contacts from 'react-native-contacts';

import Styles from './common/styles';
import Values from './common/values';
import Texts from './common/texts';
import Cache from './common/cache';


import Loading from './components/Loading';

import Intro from './intro';
import SignIn from './components/authentication/sign-in';
import Email from './components/authentication/email';
import OTPVerification from './components/authentication/otp-verification';

import Profile from './components/user/Profile';

import Chats from './components/user/Chats';
import Chat from './components/user/Chat';
import NewChat from './components/user/NewChat';

import ContactsC from './components/user/Contacts';
import InviteMessage from './components/user/InviteMessage';
import InviteEmail from './components/user/InviteEmail';
import AddFriend from './components/user/AddFriend';
import AddGroup from './components/user/AddGroup';

import Tales from './components/user/Tales';
import Tale from './components/user/Tale';

import Settings from './components/user/Settings';

import connect from './connect';
import { Client } from 'bugsnag-react-native';


const reducerCreate = params => {
    const defaultReducer = Reducer(params);
    return (state, action) => {
        return defaultReducer(state, action);
    }
};

const getSceneStyle = (/* NavigationSceneRendererProps */ props, computedProps) => {
    const style = {
        flex: 1,
        backgroundColor: '#fff',
        shadowColor: null,
        shadowOffset: null,
        shadowOpacity: null,
        shadowRadius: null,
    };

    if (computedProps.isActive) {
        style.marginTop = computedProps.hideNavBar ? 0 : 64;
        style.marginBottom = computedProps.hideTabBar ? 0 : 64;
    }
    return style;
};

class TabIcon extends Component {
    render() {
        return (
            <Text style={{ color: this.props.selected ? 'red' : 'black' }}>{this.props.title}</Text>
        );
    }
}

class ChatIcon extends Component {
    render() {
        return (
            <View style={styles.tabMenu}>
                <Image style={styles.tabMenuIcon} source={this.props.selected ? require('./images/chat-active.png') : require('./images/chat.png')} />
                <Text style={styles.tabMenuText}>{Texts.Chats}</Text>
            </View>
        );
    };
}

class TalesIcon extends Component {
    render() {
        return (
            <View style={styles.tabMenu}>
                <Image style={styles.tabMenuIcon} source={this.props.selected ? require('./images/tales-icon-active.png') : require('./images/tales-icon.png')} />
                <Text style={styles.tabMenuText}>{Texts.Tales}</Text>
            </View>
        );
    };
}

class ContactsIcon extends Component {
    render() {
        return (
            <View style={styles.tabMenu}>
                <Image style={styles.tabMenuIcon} source={this.props.selected ? require('./images/contacts-active.png') : require('./images/contacts.png')} />
                <Text style={styles.tabMenuText}>{Texts.Contacts}</Text>
            </View>
        );
    };
}

class SettingsIcon extends Component {
    render() {
        return (
            <View style={styles.tabMenu}>
                <Image style={styles.tabMenuIcon} source={this.props.selected ? require('./images/settings-active.png') : require('./images/settings.png')} />
                <Text style={styles.tabMenuText}>{Texts.Settings}</Text>
            </View>
        );
    };
}

class MoreIcon extends Component {
    render() {
        return (
            <View style={styles.tabMenu}>
                <Image style={styles.tabMenuIcon} source={this.props.selected ? require('./images/more-active.png') : require('./images/more.png')} />
            </View>
        );
    };
}

class App extends Component {

    _user;
    _readState = 0;

    constructor(props) {
        super(props);
        // this.client = new Client('0f0ef6ee71203f156a124fd8542e0e38');

        const { connected, user } = this.props;
        this._readState = 0;
        console.log('this._readState: ', this._readState);
        this.state = {
            connected: connected,
            user: user
        }
        window.GM = {};

        this.selector = this.selector.bind(this);

        this.readStorage = this.readStorage.bind(this);

        // this.navigatorRenderScene = this.navigatorRenderScene.bind(this);

        this.readStorage();

    }


    readStorage() {

        var self = this;
        Values.getStorage(AsyncStorage, 'user').then(function (user) {
            self._user = JSON.parse(user);

            window.GM.U = self._user;
            self._readState++;

            if (!self._user) return;

            // self.getAllPhoneContacts();
            console.log('HERE self._readState: ', self._readState);
        });

        Values.getStorage(AsyncStorage, 'details').then(function (details) {
            var details = JSON.parse(details);

            window.GM.D = details;
            self._readState++;
            console.log('details self._readState: ', self._readState, details);
        });

        Values.getStorage(AsyncStorage, 'groups').then(function (groups) {
            var groups = JSON.parse(groups);

            window.GM.G = groups;
            self._readState++;
            console.log('groups self._readState: ', self._readState, groups);
        });

        Values.getStorage(AsyncStorage, 'phoneFriends').then(function (pf) {
            var pf = JSON.parse(pf);

            window.GM.pf = pf;
            self._readState++;
            console.log('phoneFriends self._readState: ', self._readState, pf);
        });

        Values.getStorage(AsyncStorage, 'friendPhones').then(function (fp) {
            var fp = JSON.parse(fp);

            window.GM.fp = fp;
            self._readState++;
            console.log('friendPhones self._readState: ', self._readState, fp);
        });

        Values.getStorage(AsyncStorage, 'nonFriends').then(function (nf) {
            var nf = JSON.parse(nf);

            window.GM.nf = nf;
            console.log('nonFriends self._readState: ', self._readState, nf);
        });


    }


    componentWillMount() {
        connect();

        this.scenes = Actions.create(
            <Scene key="root" component={createContainer(this.composer, Switch)} selector={this.selector} tabs={true}>

                <Scene key="loading" hideNavBar={true} hideTabBar={true} component={Loading} />

                <Scene key="guest" hideNavBar={true}>
                    <Scene key="Intro" component={Intro} hideNavBar={true} hideTabBar={true} />
                    <Scene key="SignIn" component={SignIn} hideNavBar={true} hideTabBar={true} />
                    <Scene key="Email" component={Email} hideNavBar={true} hideTabBar={true} />
                    <Scene key="OTPVerification" component={OTPVerification} hideNavBar={true} hideTabBar={true} />

                    <Scene key="SelectList" component={SelectList} hideNavBar={true} />
                </Scene>

                <Scene key="user" tabs={true} tabBarStyle={styles.tabBarStyle} tabBarSelectedItemStyle={styles.tabBarSelectedItemStyle} hideNavBar={true}>
                    <Scene key="Chats" component={Chats} title="Chats" icon={ChatIcon} hideNavBar={true} />
                    <Scene key="Chat" component={Chat} title="Chat" hideTabBar={true} hideNavBar={true} />
                    <Scene key="NewChat" component={NewChat} title="New Chat" hideTabBar={true} hideNavBar={true} />

                    <Scene key="Tales" component={Tales} title="Tales" icon={TalesIcon} hideNavBar={true} />
                    <Scene key="Tale" component={Tale} hideTabBar={true} hideNavBar={true} />

                    <Scene key="Contacts" component={ContactsC} title="Contacts" icon={ContactsIcon} hideNavBar={true} />
                    <Scene key="InviteMessage" component={InviteMessage} title="Invite Message" hideTabBar={true} hideNavBar={true} />
                    <Scene key="InviteEmail" component={InviteEmail} title="Invite Email" hideTabBar={true} hideNavBar={true} />
                    <Scene key="AddFriend" component={AddFriend} title="Add Friend" hideTabBar={true} hideNavBar={true} />
                    <Scene key="AddGroup" component={AddGroup} title="Add Group" hideTabBar={true} hideNavBar={true} />

                    <Scene key="Settings" component={Settings} title="Settings" icon={SettingsIcon} hideNavBar={true} />
                    <Scene key="Profile" component={Profile} title="Profile" icon={MoreIcon} hideNavBar={true} />
                </Scene>

            </Scene>
        );

    }


    composer() {
        return {
            connected: Meteor.status().connected,
            user: Meteor.user()
        }
    }

    selector(data, props) {
        // return 'user';
        var self = this;
        console.log('this._readState: ', this._readState, window.GM);
        if (this._readState < 5) {
            return "loading";
        } else {
            if (data.user) {
                console.log('It is USER.', new Date())
                Cache.getContactsFromPhone(Contacts, AsyncStorage);
                console.log('It is USER.2', new Date())
                return 'user';
            } else {
                console.log('It is Guest.')
                return 'guest';
            }
            // if (window.GM && window.GM.U) {
            //     console.log('It is USER.')
            //     return 'user';
            // } else {
            //     console.log('It is Guest.')
            //     return "guest";
            // }
        }

        // if (!data.connected) {
        //     return "loading";
        // } else if (!data.user) {
        //     return "guest";
        // } else {
        //     // props.user = data.user;
        //     return "user";
        // }
    }


    componentWillReceiveProps(nextProps) {
        this.setState({
            connected: nextProps.connected,
            user: nextProps.user
        });

        if (!this.state.user && nextProps.user) {
            // Actions.Profile();

            window.GM.U = nextProps.user;
            Cache.getGroups(Meteor, AsyncStorage);
            Cache.getFriends(Meteor, AsyncStorage);
        }
    }


    seducer(props) {
        let scenesMap;

        if (props.scenes) {
            scenesMap = props.scenes;
        } else {
            let scenes = props.children;

            if (Array.isArray(props.children) || props.children.props.component) {
                scenes = (
                    <Scene
                        key="__root"
                        hideNav
                        {...this.props}
                        >
                        {props.children}
                    </Scene>
                );
            }
            scenesMap = Actions.create(scenes, props.wrapBy);
        }
    }

    render() {

        return <Router createReducer={reducerCreate} getSceneStyle={getSceneStyle} scenes={this.scenes} />;
    }

}




const styles = StyleSheet.create({
    tabBarStyle: {
        backgroundColor: Values.colors.background,
        borderColor: Values.colors.gray,
        borderTopWidth: 1,
        height: 63
    },
    tabBarSelectedItemStyle: {
    },
    tabMenu: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    tabMenuIcon: {
        width: 32,
        height: 32
    },
    tabMenuText: {
        paddingTop: 2,
        fontSize: 12
    }
});

export default createContainer(params => {
    return {
        connected: Meteor.status().connected,
        user: Meteor.user()
    }
}, App);