import Values from './values';

var _suryaContacts = [{
    recordID: 1,
    company: "",
    emailAddresses: [{
        label: "work",
        email: "carl-jung@example.com",
    }],
    familyName: "Rathan",
    givenName: "George",
    jobTitle: "",
    middleName: "",
    phoneNumbers: [{
        label: "mobile",
        number: "9703 444 537",
    }],
    thumbnailPath: "",
}, {
    recordID: 2,
    company: "",
    emailAddresses: [],
    familyName: "Silpa",
    givenName: "Idam",
    jobTitle: "",
    middleName: "",
    phoneNumbers: [{
        label: "mobile",
        number: "(888) 507-4572",
    }],
    thumbnailPath: "",
}, {
    recordID: 3,
    company: "",
    emailAddresses: [],
    familyName: "Rajesh",
    givenName: "Gandreddi",
    jobTitle: "",
    middleName: "",
    phoneNumbers: [{
        label: "mobile",
        number: "(988) 567-2797",
    }],
    thumbnailPath: "",
}];
var _rathanContacts = [{
    recordID: 1,
    company: "",
    emailAddresses: [],
    familyName: "Surya Chandra Rao",
    givenName: "Gandreddi",
    jobTitle: "",
    middleName: "",
    phoneNumbers: [{
        label: "mobile",
        number: "81422 34448",
    }],
    thumbnailPath: "",
}, {
    recordID: 2,
    company: "",
    emailAddresses: [],
    familyName: "Silpa",
    givenName: "Idam",
    jobTitle: "",
    middleName: "",
    phoneNumbers: [{
        label: "mobile",
        number: "(888) 507-4572",
    }],
    thumbnailPath: "",
}, {
    recordID: 3,
    company: "",
    emailAddresses: [],
    familyName: "Satyavathi",
    givenName: "Gandreddi",
    jobTitle: "",
    middleName: "",
    phoneNumbers: [{
        label: "mobile",
        number: "(905) 946-6827",
    }],
    thumbnailPath: "",
}];


const cache = {

    _suryaContacts: _suryaContacts,
    _rathanContacts: _rathanContacts,

    getContactsFromPhone: function (Contacts, AsyncStorage) {
        var user = window.GM.U;

        if (!user) return;

        Contacts.getAll((err, phoneContacts) => {
            console.log('phoneContacts: ', phoneContacts);
            // phoneContacts = _suryaContacts;
            if (err && err.type === 'permissionDenied') {
                // alert('No Permission to Access Contacts');
            } else {
                var phoneNameMap = [];
                var _Details = {};

                var userCode = user.profile.code;
                var userPhone = user.profile.phone;
                _Details[userCode + userPhone] = { n: 'You' };

                for (var phoneContact of phoneContacts) {
                    var name = phoneContact.givenName;
                    if (phoneContact.middleName) {
                        name += ' ' + phoneContact.middleName;
                    }
                    if (phoneContact.familyName) {
                        name += ' ' + phoneContact.familyName;
                    }
                    name = name.trim();

                    for (var phoneNumber of phoneContact.phoneNumbers) {

                        var phone = phoneNumber.number;
                        phone = phone.replace(/[^0-9]/g, '').replace(/^0+/g, '');
                        if (phone.length == userPhone.length) {
                            phone = userCode + phone;
                        } else if (phone.length == userCode.length + userPhone.length) {
                            //Nothing already country code is there.
                        } else {

                        }

                        var value = { n: name, p: phoneNumber.number, r: phoneContact.r };
                        phoneNameMap.push([
                            phone, JSON.stringify(value)
                        ]);

                        _Details[phone] = value;
                    }
                }

                console.log('_Details: ', _Details);

                window.GM.D = _Details;

                Values.setStorage(AsyncStorage, 'details', _Details);
            }
        });
    },

    getFriends: function (Meteor, AsyncStorage) {
        var user = window.GM.U;

        if (!user) return;

        var self = this;

        Meteor.call('GetAllContacts', function (error, contacts) {
            console.log('GetAllContacts: ', error, contacts);
            if (error) {

            } else {
                self.setFriends(contacts, AsyncStorage);


            }
        });
    },

    setFriends: function (contacts, AsyncStorage) {

        if (contacts.length == 0) {

        } else {
            var fp = {};
            var pf = {};
            var nf = [];

            for (let contact of contacts) {
                if (contact._F) {
                    fp[contact._F] = contact.p;
                    pf[contact.p] = contact._F;
                } else {
                    nf.push(contact.p);
                }
            }

            Values.setStorage(AsyncStorage, 'phoneFriends', pf);
            Values.setStorage(AsyncStorage, 'friendPhones', fp);
            Values.setStorage(AsyncStorage, 'nonFriends', nf);

            window.GM.pf = pf;
            window.GM.fp = fp;
            window.GM.nf = nf;
        }

    },

    getGroups: function (Meteor, AsyncStorage) {
        var self = this;
        var user = window.GM.U;

        if (!user) return;

        var userId = window.GM.U._id;


        Meteor.call('GetAllGroups', function (error, groups) {
            console.log('GetAllGroups: ', userId, error, groups);
            if (error) {

            } else {
                self.setGroups(userId, groups, AsyncStorage);
            }
        });
    },

    setGroups: function (userId, groups, AsyncStorage) {

        var g = {};
        for (let group of groups) {

            if (group.i) {      //Internal Group
                if (userId == group._U[0]) {
                    g[group._id] = {
                        i: group.i,
                        U: group._U[1]
                    }

                } else {
                    g[group._id] = {
                        i: group.i,
                        U: group._U[0]
                    }

                }
            } else {
                g[group._id] = {
                    n: group.n,
                    i: group.i,
                    p: group.p,
                    U: group._U,
                    O: group._O
                }
            }
        }

        console.log('Groups: ', g);
        Values.setStorage(AsyncStorage, 'groups', g);
        window.GM.G = g;

    }


};

export default cache;