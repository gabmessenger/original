import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Dimensions
} from 'react-native';

import tinycolor from 'tinycolor2';

let { height, width } = Dimensions.get("window")


let margin = 20;
let padding = 20;

let colors = {
    background: '#effafd',

    white: '#FFF',
    black: '#000',

    //ios colors
    blue: '#0076FF',
    lightBlue: '#54C7FC',
    green: '#44DB5E',
    red: '#FF3824',
    yellow: '#FFCD00',
    orange: '#FF9600',
    darkPink: '#FF2851',
    darkGray: '#8E8E93',
};

let fontFamily = 'Roboto,Helvetica Neue,sans-serif';

colors.darkBlue = tinycolor(colors.blue).darken(5).toString();
colors.gray = tinycolor(colors.darkGray).lighten(5).toString();
colors.lightGray = tinycolor(colors.darkGray).lighten(20).toString();
colors.lightestGray = tinycolor(colors.darkGray).lighten(35).toString();
colors.lightBackground = tinycolor(colors.background).lighten(1).toString();

colors.messageGreen = tinycolor(colors.green).lighten(35).toString();
colors.messageGray = tinycolor(colors.black).lighten(92).toString();
console.log('colors: ', colors);
const values = {
    bugsnag: '0f0ef6ee71203f156a124fd8542e0e38',
    
    width: width,
    height: height,

    margin: margin,
    padding: padding,

    //Colors
    colors: colors,

    icon: {
        size: 24
    },

    //Font Sizes
    heading: {
        fontSize: 17,
        lineHeight: 24,
        color: tinycolor(colors.black).lighten(10).toString(),
        borderColor: tinycolor(colors.black).lighten(80).toString(),
    },
    label: {
        fontSize: 15,
        lineHeight: 20,
        fontWeight: '400',
        color: tinycolor(colors.black).lighten(40).toString()
    },
    text: {
        fontSize: 16,
        lineHeight: 22,
        color: tinycolor(colors.black).lighten(20).toString()
    },
    input: {
        fontSize: 16,
        fullWidth: width - 2 * margin,
        color: tinycolor(colors.black).lighten(18).toString(),
        backgroundColor: tinycolor(colors.background).lighten(1).toString(),
        borderColor: tinycolor(colors.black).lighten(80).toString(),
        underlayColor: tinycolor(colors.black).lighten(90).toString(),
        placeholderTextColor: tinycolor(colors.black).lighten(60).toString()
    },
    button: {
        fontSize: 16,
        color: tinycolor(colors.white).toString(),
        backgroundColor: tinycolor(colors.blue).toString(),
        borderColor: tinycolor(colors.blue).darken(10).toString(),
        underlayColor: tinycolor(colors.blue).lighten(10).toString(),
    },

    AWS: {
        S3: {
            bucket: "gab-messenger-chat",
            region: "us-west-2",
            AccessKey: "AKIAIN5LJ2L4V33GT4OA",
            SecretKey: "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",
            folders: {
                pic: 'dev/', //'pics/'
                chats: 'dev/', //'chats/'
            }
        }
    },

    getStorage: function (storage, name) {
        return storage.getItem(name);
    },

    setStorage: function (storage, name, data) {
        return storage.setItem(name, JSON.stringify(data));
    },

    removeStorage: function (storage, name) {
        return storage.removeItem(name);
    },

    getValues: function () {
        var User = window.GM.U;

        if (!User) return;

        var _ = {};
        var details = window.GM.D;
        var User = window.GM.U;
        console.log('User: ', User);
        
        _[User._id] = {
            n: 'You'
        }

        console.log('details: ', details);
        var friends = window.GM.fp;
        console.log('friends: ', friends);
        for (var friend in friends) {
            if (friends[friend] && details[friends[friend]]) {
                var value = details[friends[friend]];
                value._ = true; //Contact
                _[friend] = value;
            }
        }

        var groups = window.GM.G;
        console.log('groups: ', groups);
        for (var group in groups) {
            if (groups[group]) {
                var value = groups[group];
                value._ = false; //Group
                _[group] = value;
            }
        }

        console.log('');
        console.log('');
        console.log('_:', _);

        return _;
    },

    getFriends: function () {
        var _ = [];
        var friends = window.GM.fp;
        console.log('friends: ', friends);
        for (var friend in friends) {
            _.push(friend);
        }


        console.log('');
        console.log('');
        console.log('friends _:', _);
        
        return _; 
    },

    getGroupIds: function () {
        var groupIds = [];

        var groups = window.GM.G;
        for (var group in groups) {
            groupIds.push(group);
        }
        console.log('groupIds: ', groupIds);
        return groupIds;
    }

};

export default values;



