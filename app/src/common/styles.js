//http://ivomynttinen.com/blog/ios-design-guidelines

import React, { Component } from 'react';
import { StyleSheet, PixelRatio } from 'react-native';

import Values from './values';

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        padding: Values.padding,
        paddingBottom: 0,
        backgroundColor: Values.colors.background,
        height: Values.height
    },
    viewCenterContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Values.colors.background,
        height: Values.height
    },

    centerText: {
        fontSize: 14,
        opacity: 0.8
    },

    centerContainer: {
        flex: 1,
        
        
        padding: Values.padding,
    },
    fullContainer: {
        flex: 1,
        backgroundColor: Values.colors.background,
        padding: Values.padding,
        height: Values.height
    },

    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },

    titleContainer: {
        paddingTop: 5,
        paddingBottom: 8,
        marginLeft: -1 * Values.margin,
        marginRight: -1 * Values.margin
    },
    titleContents: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },

    bodyContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bodyContents: {
    },

    footerContainer: {
        height: 40
        , backgroundColor: '#00f'
    },
    footerContents: {
    },

    titleText: {
        color: Values.heading.color,
        fontSize: Values.heading.fontSize,
        lineHeight: Values.heading.lineHeight,
        textAlign: 'center',
        flex: 1
    },

    instructions: {
        color: Values.heading.color,
        fontSize: Values.text.fontSize + 1,
        lineHeight: Values.text.lineHeight + 3,
        textAlign: 'center',
        marginBottom: Values.margin
    },

    input: {
        borderColor: '#CCCCCC',
        borderWidth: 1,
        borderRadius: 3,
        padding: 10,
        marginTop: 5,
        marginBottom: 10,
        alignSelf: 'center',
        height: 40,
        width: 300,
        fontSize: Values.input.fontSize
    },

    label: {
        fontSize: Values.label.fontSize,
        color: Values.label.color,
        fontWeight: Values.label.fontWeight
    },

    error: {
        color: '#BB0000',
        fontSize: Values.text.fontSize,
        alignSelf: 'center'
    },

    backArrow: {
        marginTop: -4,
        marginRight: -6
    },

    pickUpListItem: {
        padding: 10,
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: Values.input.borderColor,
    },
    pickUpListItemName: {
        flex: 1,
        fontSize: 16
    },
    pickUpListItemValue: {
        fontWeight: "500",
        fontSize: Values.text.fontSize,
    },
    pickUpListItemSelected: {
        paddingLeft: 5,
        marginTop: -5
    },
    pickUpListItemNotSelected: {
        paddingLeft: 5,
        width: 31
    },

    profilePicContainer: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    profilePic: {
        borderRadius: 60,
        width: 120,
        height: 120
    },
    topView: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: Values.width,
        height: Values.height
    },
    hideTopViewToRight: {
        // width: 0,
        //top: -10,
        left: Values.width + 100
    }
})


export default styles; //styles);
