import {Meteor} from 'meteor/meteor';
import { Logins } from '../../both/collections/logins';
import { Accounts } from 'meteor/accounts-base'

Meteor.methods({
    SaveProfile: function (name, email) {
        console.log(Meteor.userId, name, email);
        var userId = Meteor.userId();

        var setting = {};
        if (name) {
            setting['profile.name'] = name;
        }
        if (email) {
            setting['emails'] = [{ address: email }];
        }
        Meteor.users.update({ _id: userId }, {
            $set: setting
        }, function (error, updated) {
            console.log('User Update: ', error, updated);
            if (error) {

            } else {

            }

        });
    },

    SaveProfilePic: function (pic) {
        var userId = Meteor.userId();
        Meteor.users.update({ _id: userId }, {
            $set: {
                "profile.pic": pic
            }
        }, function (error, updated) {
            console.log('User Profile Pic: ', error, updated);
            if (error) {

            } else {

            }

        });
    }
});