import { Email } from 'meteor/email';
import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Logins } from '../../both/collections/logins';

Meteor.methods({
    InviteEmail: function(to, subject, content) {
        console.log('Sending Invite Email....', to, subject, content);

        Email.send({
            to: to,
            from: Meteor.settings.Email.from,
            subject: subject,
            text: content
        });

    },

    InviteMessage: function(tos, message) {
        console.log('InviteMessage....', tos, message);
        AWS.config.update({
            accessKeyId: Meteor.settings.AWS.SMS.Access,
            secretAccessKey: Meteor.settings.AWS.SMS.Secret,
            region: "us-west-2"
        });

        var sns = new AWS.SNS();

        for (var i = 0; i < tos.length; i++) {
            var params = {
                Message: message,
                MessageStructure: 'string',
                PhoneNumber: tos[i]
            };
            console.log('params: ', params);

            sns.publish(params, function(err, data) {
                if (err) {
                    // throw new Meteor.Error("invite-message", err);
                } else {
                    console.log(data);
                }
            });
        }

    }
})