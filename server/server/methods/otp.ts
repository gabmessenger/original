import { Meteor } from 'meteor/meteor';
import { Logins } from '../../both/collections/logins';
import { Accounts } from 'meteor/accounts-base'

Meteor.methods({
    VerifyOTP: function (code, phone, otp, duid) {

        console.log(code, phone, otp, 'VerifyOTP....');

        var logins = Logins.find({
            code: code,
            phone: phone,
            otp: otp,
            status: 'sent'
        }).fetch();

        if (logins && logins.length == 1) {
            console.log('logins: ', logins);
            var login = logins[0];

            var user = Meteor.users.findOne({ username: login.code + login.phone });

            if (user) {
                console.log('Password Change.');
                Accounts.setPassword(user._id, login.otp);

                var devices = user.profile.devices;
                if (devices.indexOf(duid) >= 0) {
                    //Already Exists.

                } else {
                    devices.push(duid);
                    Meteor.users.update({ _id: user._id }, {
                        $set: {
                            "profile.devices": devices
                        }
                    }, function (error, updated) {
                        console.log('User Update: ', error, updated);
                        if (error) {

                        } else {

                        }

                    });
                }
            } else {
                console.log('Create User.');
                var options = {
                    username: login.code + login.phone,
                    password: login.otp,
                    profile: {
                        username: login.code.replace(/[^0-9]/g, '').replace(/^0+/g, '') + login.phone.replace(/[^0-9]/g, '').replace(/^0+/g, ''),
                        code: login.code.replace(/[^0-9]/g, '').replace(/^0+/g, ''),
                        phone: login.phone.replace(/[^0-9]/g, '').replace(/^0+/g, ''),
                        // active: duid,
                        devices: [duid],
                        imports: []
                    }
                }
                if (login.email) {
                    options['email'] = login.email;
                }
                Accounts.createUser(options);
            }

            Logins.update(login._id, {
                $set: {
                    status: 'verified',
                    verifiedAt: new Date()
                }
            }, function (error, updated) {
                if (error) {
                    throw new Meteor.Error("login-error", "Could not login, please try again after some time.");
                } else {
                    console.log('otp: ', user);
                }
            });

            return true;
        } else {
            return false;
        }
    }
});