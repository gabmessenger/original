import {Email} from 'meteor/email';
import {check} from 'meteor/check';
import {Meteor} from 'meteor/meteor';
import { Logins } from '../../both/collections/logins';

Meteor.methods({
    sendEmail: function (id, email) {
        console.log('Sending Email....', id, email);

        var login = Logins.findOne(id);
        if (login) {
            Logins.update(login._id, {
                $set: {
                    email: email,
                    emailAt: new Date()
                }
            }, function (error, updated) {
                if (error) {
                    throw new Meteor.Error("email-error", "Could not sent Email, please try again after some time.");
                } else {
                    Email.send({
                        to: email,
                        from: Meteor.settings.Email.from,
                        subject: "Gab Messenger OTP",
                        text: "Gab Messenger OTP " + login.otp
                    });
                }
            }
            );


        }
    }
})