import { Meteor } from 'meteor/meteor';
import { Tales } from '../../both/collections/tales';

Meteor.methods({
    TopTales: function () {
        return Tales.find({}).fetch();

    },
    GetTale: function (id) {
        var tale = Tales.findOne(id);
        console.log('tale: ', id, tale);
        return tale;
    }
});

Meteor.publish('TopTales', () => {
  return Tales.find();
});
