import { Meteor } from 'meteor/meteor';
import { Overviews } from '../../both/collections/overviews';
import { Overview } from '../../both/models/overview';

Meteor.methods({

    UpdateOverview: function (chat) {
        console.log('UpdateOverview ....', chat);

        //Group Message - write to all the Group Members
        // if (chat.g) {

        // } else {

            let existingOverviews = Overviews.find({ _G: chat._G }).fetch();
            if (existingOverviews && existingOverviews.length > 0) {
                let existingOverview = existingOverviews[0];

                Overviews.update({
                    _id: existingOverview._id
                }, {
                        $set: {
                            _F: chat._F,
                            m: chat.m,
                            a: new Date()
                        }
                })
            }

        // }

    },

    CreateOverview: function (groupId) {
        return Overviews.insert({ _G: groupId });
    }

});

// Overviews.insert({_G: '329A8mXxPMjHN4vfx', _F: '4HAx4NRHsmPhAuTME', m: 'Testing 2', a: new Date()})

Meteor.publish('Overviews', () => {
    return Overviews.find({  }, { sort: { a: -1 } });
});
