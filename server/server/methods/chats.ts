import { Meteor } from 'meteor/meteor';
import { Chats } from '../../both/collections/chats';
import { Chat } from '../../both/models/chat';

Meteor.methods({

    SendMessage: function (group, to, message, link) {
        console.log('SendMessage ....', group, to, message, link);
        let from = Meteor.userId();

        let chat: Chat = {
            _F: from,
            _G: group,
            m: message,
            l: link,
            a: new Date()
        };

        var chatId = Chats.insert(chat);

        Meteor.call("UpdateOverview", chat)

        return chatId;

    },

})

Meteor.publish('Chats', () => {
    return Chats.find({}, { sort: { a: -1 } });
  //_F: F, _T: T  
});
