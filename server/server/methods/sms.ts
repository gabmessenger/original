import {Meteor} from 'meteor/meteor';
import {generate} from 'randomstring';
import { Logins } from '../../both/collections/logins';

Meteor.methods({
    SMS: function (code, phone, duid) {

        // console.log(code, phone, 'Sending SMS....', Meteor.settings);
        AWS.config.update({
            accessKeyId: Meteor.settings.AWS.SMS.Access,
            secretAccessKey: Meteor.settings.AWS.SMS.Secret,
            region: "us-west-2"
        });

        var sns = new AWS.SNS();

        var otp = generate({
            length: 6,
            charset: 'numeric'
        });

        // console.log('randomCode: ', otp);

        var params = {
            Message: 'Gab-Messenger OTP ' + otp,
            MessageStructure: 'string',
            PhoneNumber: code + phone
        };
        console.log('params: ', params);


        var login = {
            code: code,
            phone: phone,
            otp: otp,
            status: 'sent',
            at: new Date(),
            duid: duid
        };
        return Logins.insert(login, function (error, _id) {
            if (error) {
                throw new Meteor.Error("account-creation", "Could not send the SMS please try again after some time.");
            } else {
                if (code && phone) {
                    sns.publish(params, function (err, data) {
                        if (err) {
                            throw new Meteor.Error("account-creation", "Could not send the SMS please try again after some time.");
                        } else {
                            console.log(data);
                            return _id;
                        }
                    });
                }
            }
        });

    }
});