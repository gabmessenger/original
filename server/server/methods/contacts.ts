import { Meteor } from 'meteor/meteor';
import { Contacts } from '../../both/collections/contacts';
import { Contact } from '../../both/models/contact';

Meteor.methods({
    ImportAllContacts: function (device, allContacts) {
        console.log('ImportAllContacts:', allContacts);
        var userId: string = Meteor.userId();

        var user: any = Meteor.user();

        var userCode = user.profile.code;
        var userPhone = user.profile.phone;

        if (user.profile.active == device) {

        } else {        //New Device Delete all olds and import all.
            console.log('allContacts B:', allContacts);

            //All Phone Numbers to Get their User Ids
            let allFriendNumbers: string[] = [];

            for (let allContact of allContacts) {
                var phone = allContact.p;
                phone = phone.replace(/[^0-9]/g, '').replace(/^0+/g, '');
                if (phone.length == userPhone.length) {
                    phone = userCode + phone;
                } else if (phone.length == userCode.length + userPhone.length) {
                    //Nothing already country code is there.
                } else {

                }
                allContact.p = phone;
                allFriendNumbers.push(phone);
            }

            console.log('allContacts A:', allContacts);

            console.log('allFriendNumbers: ', allFriendNumbers);

            var friends = Meteor.users.find(
                { "profile.username": { $in: allFriendNumbers } },
                { fields: { "username": 1, "profile": 1 } }
            ).fetch();

            console.log('friends: ', friends);

            if (friends) {

                var friendsMap = {};
                for (let friend of friends) {
                    friendsMap[friend.profile.username] = friend._id;
                }

                console.log('friendsMap: ', friendsMap);

                for (let allContact of allContacts) {

                    let contact: Contact = {
                        _U: userId,
                        p: allContact.p
                    };

                    if (friendsMap[allContact.p]) {
                        contact['_F'] = friendsMap[allContact.p];
                    }

                    // contacts.push(contact);
                    Contacts.insert(contact);

                    //the other side contact is also friend
                    let friend = Contacts.findOne({ p: userCode + userPhone, _U: friendsMap[allContact.p] });
                    if (friend) {
                        console.log('friend: ');
                        Contacts.update({ _id: friend._id }, {
                            $set: {
                                _F: userId
                            }
                        });
                    }

                }
            }

            //Update Active Device
            var imports = user.profile.imports;
            if (imports.indexOf(device) >= 0) {

            } else {
                imports.push(device);
            }

            Meteor.users.update({ _id: userId }, {
                $set: {
                    "profile.imports": imports,
                    "profile.active": device
                }
            }, function (error, updated) {
                console.log('User Update: ', error, updated);
                if (error) {

                } else {

                }

            });
        }
    },

    GetAllContacts: function () {
        console.log('Meteor.userId(): ', Meteor.userId())
        return Contacts.find({ _U: Meteor.userId() }, { fields: { _F: 1, p: 1 } }).fetch();
    },

    AddFriend: function (phone) {

        var userId: string = Meteor.userId();

        var user: any = Meteor.user();
        console.log('AddFriend: ', user);

        var userCode = user.profile.code;
        var userPhone = user.profile.phone;

        phone = phone.replace(/[^0-9]/g, '').replace(/^0+/g, '');
        if (phone.length == userPhone.length) {
            phone = userCode + phone;
        } else if (phone.length == userCode.length + userPhone.length) {
            //Nothing already country code is there.
        } else {

        }

        var friends = Meteor.users.find(
            { "profile.username": phone },
            { fields: { "username": 1, "profile": 1 } }
        ).fetch();

        console.log('friends: ', friends);
        if (friends && friends.length > 0) {
            var friendUser = friends[0];

            let contact: Contact = {
                _U: userId,

                _F: friendUser._id,
                p: phone
            };

            Contacts.insert(contact);

            //the other side contact is also friend
            let friend = Contacts.findOne({ p: phone, _U: friendUser._id });
            if (friend) {
                console.log('friend: ', friend);
                Contacts.update({ _id: friend._id }, {
                    $set: {
                        _F: userId
                    }
                });
            }
        } else {
            let contact: Contact = {
                _U: userId,
                p: phone
            };

            Contacts.insert(contact);
        }



    },

    GetContact: function (U, F) {
        return Contacts.findOne({ _U: U, _F: F });
    }
});



Meteor.publish('Contacts', function() {
    var userId = this.userId;
    console.log('userId: ', userId);
    return Contacts.find({ "_U": userId })
});
