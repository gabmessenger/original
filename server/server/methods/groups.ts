import { Meteor } from 'meteor/meteor';
import { Groups } from '../../both/collections/groups';
import { Group } from '../../both/models/group';
import { Contacts } from '../../both/collections/contacts';
import { Contact } from '../../both/models/contact';

Meteor.methods({

    GetAllGroups: function () {
        // console.log('GetAllGroups: ', Meteor.userId())
        var userId = Meteor.userId();
        return Groups.find({ "_U": userId }).fetch();
    },

    //External Group    
    SaveGroup: function (name, users, picture, ) {
        var owner = Meteor.userId();
        console.log('Save Group ....', name, owner, users, picture);
        users.push(owner);
        let group: Group = {
            i: false,
            _C: owner,
            n: name,
            p: picture,
            _O: [owner],
            _U: users
        };

        let externalGroup: any = Groups.insert(group);
        if (externalGroup) {
            console.log('externalGroup: ', externalGroup);
            Meteor.call("CreateOverview", externalGroup);
        }

        // Meteor.call('UpdateGroupUsersContacts', users);

        return externalGroup;
    },

    GetInternalGroup: function (user) {
        console.log('GetInternalGroup: ', user);
        var owner = Meteor.userId();
        let groups = Groups.find({
            i: true,
            $or: [
                { _C: owner, _U: [user, owner] },
                { _C: user, _U: [owner, user] }
            ]
        }).fetch();
        console.log('groups: ', groups)
        if (groups && groups.length > 0) {
            return groups[0]._id;
        } else {
            //Does not exists, so Create that Group
            return Meteor.call("CreateInternalGroup", user);
        }
    },

    GetGroupDetails: function (groupId) {
        var owner = Meteor.userId();
        let group = Groups.findOne(groupId);
        console.log(owner, ' XYZ group: ', group);
        if (group) {
            //Internal One-on-One chat.
            if (group.i) {
                let C = group._C;
                if (owner == C) {
                    return group._O[0];
                } else {
                    return C;
                }
            } else {
                return group;
            }
        }
        return null;
    },

    CreateInternalGroup: function (user) {
        var owner = Meteor.userId();
        console.log('CreateInternalGroup Group ....', user);

        let group: Group = {
            i: true,
            _C: owner,
            _U: [user, owner]
        };

        let internalGroup: any = Groups.insert(group);

        if (internalGroup) {
            console.log('internalGroup: ', internalGroup);
            Meteor.call("CreateOverview", internalGroup);
        }

        return internalGroup;
    },

    UpdateGroupUsersContacts: function (users) {
        let owner = Meteor.userId();
        let friendUsers = Meteor.users.find({ _id: { $in: users } }, { fields: { profile: 1 } }).fetch();

        if (friendUsers) {
            let friendUser = {};
            for (let friend of friendUsers) {
                friendUser[friend._id] = friend.profile
            }

            for (let user of users) {
                if (owner != user) {
                    for (let contact of users) {
                        if (contact != user) {
                            let friend = Contacts.findOne({ _U: user, _F: contact });
                            if (friend) {
                                //Do Nothing
                            } else if (friendUser[contact]) {

                                let newContact: Contact = {
                                    _U: user,
                                    p: friendUser[contact].username,
                                    n: friendUser[contact].profile.name
                                };

                                Contacts.insert(newContact);

                            }
                        }
                    }
                }
            }
        }

    }

});

// Groups.insert({ i: false, _C: '4HAx4NRHsmPhAuTME', n: 'Auto Add Group', _U: ['4HAx4NRHsmPhAuTME', 'RGdrpPEXjMnfvMGZS'] })

Meteor.publish('Groups', function() {
    var userId = this.userId;
    console.log('userId: ', userId);
    return Groups.find({ "_U": userId })
});
