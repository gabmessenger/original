import { Component } from '@angular/core';

import template from './app.component.html';

@Component({
  selector: 'app',
  template,
  directives: []
})
export class AppComponent {
  constructor() {
  }
}
