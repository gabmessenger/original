export interface Topic {
  _id?: string;

  l: string;  //Location
  t: string;  //Topic Name

  i: number; //How Many are interested in this topic - default : 0
}

