
export interface Tale {
     _id?: string;
  
  _T: string; //Topic Id - Reference to Topics collection

  l: string;  //Link
  t: string;  //Title/Headline
  b: string;  //Brief/Description

  p: number;  //Priority
  a: Date;    //At - default: new Date()
  
  i: number;  //Interests/Likes - default : 0
  s: number;  //Shares - default : 0

}
