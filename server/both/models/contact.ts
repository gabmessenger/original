export interface Contact {
    _id?: string;

    // User Id to which this Contact belongs
    _U: string;

    //Friend User Id
    _F?: string;

    //Phone - Two Phones can refer to the same contact (in phone book) but users are different.
    p: string;

    //Name - when this contact is not yet a friend of the User (on the Phone)
    n?: string;


}
