export interface Overview {
    _id?: string;

    //Groups - group of Users 
    _G: string;

    //From User - who wrote the last message - can be null in case of Non-Group chat.
    _F?: string;

    //Message    
    m?: string;

    //Created/Updated At
    a?: Date;

}

