export interface Login {
  _id?: string;
  code: string;
  phone: string;
  otp: string;
  status: string;
  at: Date;
  email?: string;
  emailAt?: Date;
  verifiedAt?: Date;

  //Device unique
  duid: string;
}