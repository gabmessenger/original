export interface Group {
    _id?: string;

    //Internal Groups - means Groups created by App for 2 users - one-on-one chat.
    i: boolean;

    //Created User Id
    _C: string;

    //Name    
    n?: string;

    //Group Picture
    p?: string;

    //Owners
    _O?: [string];
    
    //Users
    _U: [string];

}
