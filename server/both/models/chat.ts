export interface Chat {
    _id?: string;

    //From User    
    _F: string;

    //To Group, False for User
    _G: string;
    
    //Message    
    m: string;

    //Media Link - Image/Audio/Video
    l: string;

    //Created At
    a: Date;

    //Delivered
    d: [any];

    //Read
    r: [any];

}