import {Contact} from "../models/contact";
import {Mongo} from "meteor/mongo";

export const Contacts = new Mongo.Collection<Contact>('contacts');
