import {Group} from "../models/group";
import {Mongo} from "meteor/mongo";

export const Groups = new Mongo.Collection<Group>('groups');
