import {Tale} from "../models/tale";
import {Mongo} from "meteor/mongo";

export const Tales = new Mongo.Collection<Tale>('tales');
