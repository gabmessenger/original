import {Login} from "../models/login";
import {Mongo} from "meteor/mongo";

export const Logins = new Mongo.Collection<Login>('logins');
