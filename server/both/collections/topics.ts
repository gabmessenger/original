import {Topic} from "../models/topic";
import {Mongo} from "meteor/mongo";

export const Topics = new Mongo.Collection<Topic>('topics');