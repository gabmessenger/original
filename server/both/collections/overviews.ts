import {Overview} from "../models/overview";
import {Mongo} from "meteor/mongo";

export const Overviews = new Mongo.Collection<Overview>('overviews');
